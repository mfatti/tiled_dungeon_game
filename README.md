# Tiled Dungeon Game

This is another game prototype I worked on a few years ago (~2016-2017). The code is a bit cleaner than Pyre of Gods and this is actually thread-safe (there is a race condition in the Pyre of Gods client that can cause a crash), using a custom mutex class (spinlock mutex I name FastMutex).

**Note the code isn't the neatest or best designed**

This game used a "game engine" that I rewrote rather than using the Pyre of Gods engine. GLObject here has been severely reduced in scope rather than contain shaders, rendering code, positions, rotations etc as just one example. I opted to used Freetype library here too rather than a weird custom font implementation.

I will actually be using this code (gameplay elements) for a future project.


**To compile**

I have this compiling under Pop!_OS 20.04 (so Ubuntu 20.04). This was originally developed using Codelite under Xubuntu 16.04/16.10.

The dependencies are in the makefile but for the client you'll need:
- zlib
- pthread
- GLEW
- GL
- libpng
- GLFW
- freetype

Everything is in the makefile, note it's not a very good makefile so any changes to headers will need a clean compile. Just run

> make

to compile release and

> make debug

to compile debug.

> make clean

will clean everything.

**GIFs**

This isn't as meaty as Pyre of Gods, but here are a few short clips:

[![Particles](https://i.imgur.com/ZwAKZDs.gif)](https://i.imgur.com/ZwAKZDs.gif)

[![Slash](https://i.imgur.com/CKAAD3q.gif)](https://i.imgur.com/CKAAD3q.gif)

[![Magic Missile](https://i.imgur.com/7sOvjvS.mp4)](https://i.imgur.com/7sOvjvS.mp4)

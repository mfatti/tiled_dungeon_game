#version 330 core

layout (points) in;
layout (triangle_strip) out;
layout (max_vertices = 4) out;

in Vertex
{
	vec4 colour;
	vec2 size;
} vertex[];

out vec4 finalColour;

uniform mat4 projection;

void main()
{
    finalColour = vertex[0].colour;
    
    // If our particle is invisible, stop here
    if (finalColour.a > 0.05)
    {
		vec4 pos = gl_in[0].gl_Position;
		
		// First some multiplication to (barely) speed things up
		vec2 scale = vertex[0].size * 0.5;
		vec2 neg = vec2(-scale.x, -scale.y);
		
		gl_Position = projection * vec4(pos.xy + vec2(neg.x, neg.y), pos.zw);
		EmitVertex();
		
		gl_Position = projection * vec4(pos.xy + vec2(neg.x, scale.y), pos.zw);
		EmitVertex();
		
		gl_Position = projection * vec4(pos.xy + vec2(scale.x, neg.y), pos.zw);
		EmitVertex();
		
		gl_Position = projection * vec4(pos.xy + vec2(scale.x, scale.y), pos.zw);
		EmitVertex();
		
		EndPrimitive();
    }
}

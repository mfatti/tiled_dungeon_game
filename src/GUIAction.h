#ifndef GUI_ACTION_H_
#define GUI_ACTION_H_

#include "GUIElement.h"
#include "GUIImage.h"
#include "GUIText.h"

// GUIAction class - provides the information at the bottom of the screen of actions available to the player

class GUIAction : public GUIElement {
public:
	GUIAction(GUIManager* guiMan, GUIElement* parent = NULL);

	void SetAction(int id, const std::string& name);
	void SetActive(bool active);
	void SetEnabled(bool enabled);

protected:
	virtual void BuildElement();

private:
	GUIImage*	m_Image;
	GUIText*	m_Name;
	bool		m_Enabled;
	bool		m_Active;
};

#endif // GUI_ACTION_H_

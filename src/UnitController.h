#ifndef UNIT_CONTROLLER_H_
#define UNIT_CONTROLLER_H_

#include "Unit.h"
#include "AbilityController.h"
#include "PathFinder.h"
#include "ControllerNames.h"
#include <map>
#include <vector>

class SceneManager;
class UnitControllerManager;
class PlayerController;
class EnemyController;

// UnitController class - manages units

class UnitController {
public:
	UnitController(SceneManager* sceneman, UnitControllerManager* man, nsControllerNames::Controller type);
	virtual ~UnitController();

	void CreateEffectsBillboard();

	void AddUnit(Unit* unit);
	Unit* CreateUnit(const std::string& name, const vector3df& pos);
	Unit* GetUnit(size_t id) { if (id >= m_Units.size()) return nullptr; return m_Units.at(id); }
	const Unit* GetUnit(size_t id) const { if (id >= m_Units.size()) return nullptr; return m_Units.at(id); }
	Unit* GetUnit(const vector2di& pos);
	Unit* GetClosestUnit(const vector3df& pos, const std::vector<Unit*>* previous = nullptr);

	size_t GetUnitCount() const { return m_Units.size(); }

	virtual void Update();
	virtual void UpdateBackground();
	virtual void TurnStart() {}
	virtual void TurnEnd() {}
	
	virtual bool MouseEvent(int button, int action, int mods) { return false; }
	virtual bool KeyboardEvent(int key, int scancode, int action, int mods) { return false; }
	virtual bool CharEvent(unsigned int codepoint) { return false; }
	virtual bool ScrollEvent(double x, double y) { return false; }

	void LoadUnitData(const std::string& filename);
	const UnitData& GetUnitData(const std::string& name) const;
	
	void PlayEffect(int id, const vector3df& pos, float speed = 5.f);
	
	virtual vector2di GetUnitPortrait(Unit* unit);
	virtual void GetUnitCounts(size_t id, int& act, int& max_act, int& step) const;
	virtual void GetUnitAbility(size_t id, size_t ability, Ability& out, std::string& name) const { if (id < m_Units.size()) { name = GetUnit(id)->GetUnitData().abilities[ability]; out = AbilityController::GetAbility(name); } }
	nsUnitClass::UnitClass GetUnitClass(size_t id) const { if (id >= m_Units.size()) return nsUnitClass::melee; return m_Units.at(id)->GetUnitData().unit_class; }
	
	const std::vector<vector2di>& GetUnitGridPositions() const { return m_UnitPlaces; }
	void UpdateUnitGridPositions();

	virtual bool DamageUnit(Unit* unit, int damage, Unit* other = nullptr);
	virtual bool HealUnit(Unit* unit, int heal, Unit* other = nullptr);

	void SetTargetController(UnitController* target) { GetAbilityController().SetTargetController(target); }

	SceneManager* GetSceneManager() { return m_SceneMan; }

	nsControllerNames::Controller GetType() const { return m_Type; }

	static PlayerController* GetPlayerController() { return m_PlayerController; }
	static EnemyController* GetEnemyController() { return m_EnemyController; }

protected:
	UnitControllerManager* GetManager() { return m_Manager; }
	Billboard* GetBillboard() { return m_Billboard; }
	void SetUnitData(int id, UnitData& data);
	int Distance(Unit* one, Unit* two) const;
	int Distance(const vector3df& one, const vector3df& two) const;
	int Distance(const vector2di& one, const vector2di& two) const;
	AbilityController& GetAbilityController() { return m_AbilityController; }
	void SetUnitsDie(bool die) { m_UnitsDie = die; }

	int AddUnitAnimation(const vector2di& start, const vector2di& end, const vector2df& size);

	PathFinder& GetPathFinder() { return m_PathFinder; }
	
	void SetPlayerController(PlayerController* controller) { m_PlayerController = controller; }
	void SetEnemyController(EnemyController* controller) { m_EnemyController = controller; }

private:
	nsControllerNames::Controller		m_Type;
	std::vector<Unit*>					m_Units;
	Billboard*							m_Billboard;
	Billboard*							m_Effects;
	SceneManager*						m_SceneMan;
	UnitControllerManager*				m_Manager;
	std::map<std::string, UnitData>		m_UnitDataMap;
	std::vector<vector2di>				m_UnitPlaces;
	std::vector<std::vector<GLColour>>	m_UnitColours;
	PathFinder							m_PathFinder;
	AbilityController					m_AbilityController;
	bool								m_UnitsDie;
	
	static PlayerController*			m_PlayerController;
	static EnemyController*				m_EnemyController;
};

#endif // UNIT_CONTROLLER_H_

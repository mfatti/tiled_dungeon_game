#include "GameScene.h"
#include "StatusEffectManager.h"
#include "CameraController.h"

GameScene::GameScene()
	: m_pSceneMan(new SceneManager)
	, m_pGUIMan(new GUIManager)
	, m_UnitControllerManager(new UnitControllerManager())
	, m_fSun(0.f)
	, m_CurrentCharacter(0)
	, m_PreviousCharacter(9001)
	, m_CurrentAction(0)
	, m_PreviousAction(-1)
	, m_FPSStartTime(std::chrono::system_clock::now())
	, m_FPSFrames(0)
	, m_FPS(0.f)
{

}

void GameScene::CreateScene()
{
	// Create the scene manager
	m_pSceneMan->Initialise();

	// Create the camera
	m_pSceneMan->AddCamera();
	Singleton<CameraController>::Instance().SetCamera(&m_pSceneMan->GetActiveCamera());

	// Create the GUI manager
	m_pGUIMan->Initialise();

	// Load definitions
	ParticleDefinitionLibrary::LoadDefinitions("data/particles.data");
	AbilityController::LoadAbilities("data/abilities.data");

	// Create the level
	m_pSceneMan->GetTerrain().GenerateAreaLevel();

	// At the start of a new level, reset the global unit count
	// We are using the IDs 0, 1 and 2 in PlayerController for the lighting (first three at least)
	Unit::ResetGlobalUnits();

	// Create the UnitControllers
	PlayerController* player_controller = new PlayerController(m_pSceneMan.get(), m_UnitControllerManager.get());
	m_UnitControllerManager->AddUnitController(player_controller);
	player_controller->LoadUnitData("data/players.data");
	
	// Create the three player characters
	player_controller->CreateUnit("Mage", vector3df(1.5f, 0.25f, 16.5f));
	player_controller->CreateUnit("Thug", vector3df(1.5f, 0.25f, 17.5f));
	player_controller->CreateUnit("Assassin", vector3df(2.5f, 0.25f, 15.5f));
	
	// Create some enemies
	EnemyController* enemy_controller = new EnemyController(m_pSceneMan.get(), m_UnitControllerManager.get());
	m_UnitControllerManager->AddUnitController(enemy_controller);
	enemy_controller->LoadUnitData("data/enemies.data");
	
	// Make the controllers hate each other
	player_controller->SetTargetController(enemy_controller);
	enemy_controller->SetTargetController(player_controller);
	
	const std::vector<vector2di>& spawns = m_pSceneMan->GetTerrain().GetSpawnPoints();
	srand(42);
	for (auto spawn : spawns) {
		std::string name("Rat");
		switch (rand() % 3)
		{
			case 0:
				name = "Rat";
				break;
			case 1:
				name = "Spider";
				break;
			case 2:
				name = "Blood Mage";
				break;
			default:
				break;
		}
		
		enemy_controller->CreateUnit(name, vector3df(spawn.x + 0.5f, 0.25f, spawn.y + 0.5f));
	}

	// Create the effects billboards
	player_controller->CreateEffectsBillboard();
	enemy_controller->CreateEffectsBillboard();

	// Player's turn first
	player_controller->TurnStart();

	tmp = m_pGUIMan->CreateText(vector2df(16.f, 1050.f), "Generic Dungeon Game v0.01", "media/NotoSans-Bold.ttf:14");
	tmp->SetShadowed();
	
	// Create the movement steps GUI text
	m_ControllerMouseText = m_pGUIMan->CreateText(vector2df(), "0", "media/NotoSans-Bold.ttf:20");
	m_ControllerMouseText->SetShadowed();
	//m_ControllerMouseText->SetVisible(false);
	
	// Character panes
	m_Panes.push_back(m_pGUIMan->CreateCharacterPane(vector2df(0.f, 96.f), player_controller->GetUnit(0)->GetName()));
	GUIImage* portrait = m_pGUIMan->CreateImage("media/characters.png", vector2df(16.f, 20.f), vector2df(96.f, 96.f), m_Panes.at(0));
	portrait->SetSubImage(player_controller->GetUnitPortrait(player_controller->GetUnit(0)), vector2df(8.f, 8.f));
	
	m_Panes.push_back(m_pGUIMan->CreateCharacterPane(vector2df(0.f, 244.f), player_controller->GetUnit(1)->GetName()));
	portrait = m_pGUIMan->CreateImage("media/characters.png", vector2df(16.f, 20.f), vector2df(96.f, 96.f), m_Panes.at(1));
	portrait->SetSubImage(player_controller->GetUnitPortrait(player_controller->GetUnit(1)), vector2df(8.f, 8.f));
	
	m_Panes.push_back(m_pGUIMan->CreateCharacterPane(vector2df(0.f, 392.f), player_controller->GetUnit(2)->GetName()));
	portrait = m_pGUIMan->CreateImage("media/characters.png", vector2df(16.f, 20.f), vector2df(96.f, 96.f), m_Panes.at(2));
	portrait->SetSubImage(player_controller->GetUnitPortrait(player_controller->GetUnit(2)), vector2df(8.f, 8.f));
	
	// Actions
	const vector2di screen_size(GET_SCREEN_SIZE);
	m_Actions.push_back(m_pGUIMan->CreateAction(vector2df(screen_size.x / 2 - 404.f, screen_size.y - 128.f)));
	
	m_Actions.push_back(m_pGUIMan->CreateAction(vector2df(screen_size.x / 2 - 268.f, screen_size.y - 128.f)));
	m_Actions.at(1)->SetAction(1, "Ability 1");
	
	m_Actions.push_back(m_pGUIMan->CreateAction(vector2df(screen_size.x / 2 - 132.f, screen_size.y - 128.f)));
	m_Actions.at(2)->SetAction(2, "Ability 2");
	
	m_Actions.push_back(m_pGUIMan->CreateAction(vector2df(screen_size.x / 2 + 4.f, screen_size.y - 128.f)));
	m_Actions.at(3)->SetAction(3, "Ability 3");
	
	m_Actions.push_back(m_pGUIMan->CreateAction(vector2df(screen_size.x / 2 + 140.f, screen_size.y - 128.f)));
	m_Actions.at(4)->SetAction(4, "Ability 4");
	
	m_Actions.push_back(m_pGUIMan->CreateAction(vector2df(screen_size.x / 2 + 276.f, screen_size.y - 128.f)));
	m_Actions.at(5)->SetAction(3, "Special");
	
	// For the FPS counter
	m_FPSText = m_pGUIMan->CreateText(vector2df(16.f, 16.f), "FPS: OVER 9000", "media/NotoSans-Bold.ttf:14");
	m_FPSText->SetShadowed();
	
	// Create a test exit button
	/*GUIButton* button = m_pGUIMan->CreateButton(vector2df(20.f, 20.f), vector2df(200.f, 50.f), "Press Me", "media/NotoSans-Bold.ttf:12");
	button->SetFunction([=](GUIElement* e) -> bool {
		std::cout << "Quit has been executed. GameScene: " << this << std::endl;
		return true;
	});
	std::cout << "GameScene: " << this << std::endl;*/
}

void GameScene::Run()
{
	// Update and render the scene
	m_pSceneMan->Update();
	m_UnitControllerManager->Update();
	Singleton<StatusEffectManager>::Instance().UpdateEffects();
	m_pGUIMan->Update();

	UpdateCharacterGUI();

	m_pSceneMan->Render();
	m_pGUIMan->Render();
	
	// Do FPS calculations
	++m_FPSFrames;
	float time_diff = (std::chrono::system_clock::now() - m_FPSStartTime).count() / 1000000000.f;
	if (time_diff > 0.25f && m_FPSFrames > 10)
	{
		m_FPS = static_cast<float>(m_FPSFrames) / time_diff;
		m_FPSStartTime = std::chrono::system_clock::now();
		
		m_FPSText->SetText("FPS: " + std::to_string(static_cast<int>(round(m_FPS))));
		m_FPSFrames = 0;
	}
}

void GameScene::UpdateCharacterGUI()
{
	PlayerController* player_controller = UnitController::GetPlayerController();
	if (player_controller)
	{
		// Update the text following the mouse
		m_ControllerMouseText->SetPosition(GET_CURSOR_POSITION + vector2df(15.f, 5.f));
		m_ControllerMouseText->SetText(player_controller->GetMouseText());
		
		// Get details on our attacks
		m_CurrentCharacter = player_controller->GetCurrentCharacter();
		Ability abilities[4];
		
		// Set visibility based on active controller
		for (size_t i = 0; i < 6; ++i) {
			m_Actions.at(i)->SetVisible(m_UnitControllerManager->GetCurrentIndex() == 0);
		}
		
		// Update the character panes
		if (m_CurrentCharacter != m_PreviousCharacter)
		{
			std::string ability_names[4];
			
			if (m_PreviousCharacter < m_Panes.size())
			{
				m_Panes.at(m_PreviousCharacter)->SetActive(false);
			}
			
			m_Panes.at(m_CurrentCharacter)->SetActive(true);
			
			// We've changed characters, update the actions bar too
			nsUnitClass::UnitClass unit_class = player_controller->GetUnitClass(m_CurrentCharacter);
			
			for (int i = 0; i < 4; ++i) {
				player_controller->GetUnitAbility(m_CurrentCharacter, i, abilities[i], ability_names[i]);
				UpdateActionAbility(i, abilities[i], ability_names[i], unit_class);
			}
		}
		m_PreviousCharacter = m_CurrentCharacter;
		
		// Indicator counts
		int act, max_act, step;
		player_controller->GetUnitCounts(0, act, max_act, step);
		m_Panes.at(0)->SetActions(act, max_act);
		
		player_controller->GetUnitCounts(1, act, max_act, step);
		m_Panes.at(1)->SetActions(act, max_act);
		
		player_controller->GetUnitCounts(2, act, max_act, step);
		m_Panes.at(2)->SetActions(act, max_act);
		
		// Health bars
		int hp, maxhp;
		player_controller->GetUnitHealth(0, hp, maxhp);
		m_Panes.at(0)->SetHealth(hp, maxhp);
		
		player_controller->GetUnitHealth(1, hp, maxhp);
		m_Panes.at(1)->SetHealth(hp, maxhp);
		
		player_controller->GetUnitHealth(2, hp, maxhp);
		m_Panes.at(2)->SetHealth(hp, maxhp);
		
		// Update the actions
		m_CurrentAction = static_cast<int>(player_controller->GetMode());
		if (m_CurrentAction != m_PreviousAction)
		{
			if (m_PreviousAction > -1 && m_PreviousAction < static_cast<int>(m_Actions.size()))
			{
				m_Actions.at(m_PreviousAction)->SetActive(false);
			}
			
			if (m_CurrentAction < static_cast<int>(m_Actions.size())) // Won't need this check when we get everything implemented
			{
				m_Actions.at(m_CurrentAction)->SetActive(true);
			}
		}
		m_PreviousAction = m_CurrentAction;
		
		// Set actions active based on whether we have the points to use them
		player_controller->GetUnitCounts(m_CurrentCharacter, act, max_act, step);
		//m_Actions.at(1)->SetEnabled(act >= attack[0].cost);
		//m_Actions.at(2)->SetEnabled(act >= attack[1].cost);
	}
}

void GameScene::UpdateActionAbility(size_t id, Ability& ability, const std::string& name, nsUnitClass::UnitClass unit_class)
{
	// Set the action icons / text based on the given attack
	GUIAction* action = m_Actions.at(id + 1);
	
	// Firstly set the main icon
	action->SetAction(static_cast<int>(unit_class) + 1, name); // will get weapon name? (DUNNO WHAT I'M DOING FOR NAMES OF ATTACKS)
	
	/// TODO: Add icons/text for attack cost, damage, range, area, type etc
	
}

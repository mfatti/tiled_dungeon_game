#ifndef TERRAIN_H_
#define TERRAIN_H_

#include "TerrainChunkMesh.h"
#include "TerrainData.h"
#include "GLTexture.h"
#include "AABB.h"
#include "Line.h"
#include "PerlinNoise.h"
#include "zlib.h"
#include <map>
#include <array>
#include <thread>
#include <queue>
#include <atomic>
#include <chrono>
#include <memory>

#define AREA_SIZE 32
#define MESH_SIZE 16
#define MESH_COUNT AREA_SIZE / MESH_SIZE

typedef std::array<Tile, AREA_SIZE * AREA_SIZE> TileArray;
typedef std::array<TileEntity, AREA_SIZE * AREA_SIZE> TileEntityArray;
typedef std::array<std::unique_ptr<Mesh<GLVertex>>, MESH_COUNT * MESH_COUNT> EntityMeshArray;

struct AreaLevel {
	TileArray tiles;
	TileEntityArray entities;
};

struct Area {
	std::string name;
	std::vector<AreaLevel> levels;
};

class SceneNode;

class ViewFrustum;
class SceneManager;

// Terrain class - this class holds all data of the terrain and handles manipulation

class Terrain {
public:
	Terrain(SceneManager* sceneMan);
	~Terrain();

	void Update(const vector3df& position, const ViewFrustum& frustum);
	const std::vector<MeshBase*>& GetRenderList() const;

	void SetTexture(const GLTexture* tex);
	const GLTexture* GetTexture() const;

	void GenerateAreaLevel();

	bool DoesLineCollideWithTerrain(const line3df& ray, vector3df* out = nullptr) const;

	float GetHeightBelow(const vector3df& pos) const;

	const Tile& GetTile(const vector3df& pos) const;
	const Tile& GetTile(const vector2di& pos) const;
	const Tile& GetTile(int x, int y) const;
	const TileArray& GetTiles() const;
	void SetTile(const Tile& tile, int x, int y);
	void SetTile(nsTile::Tile tile, int x, int y);

	const TileEntity& GetTileEntity(const vector3df& pos) const;
	const TileEntity& GetTileEntity(const vector2di& pos) const;
	const TileEntity& GetTileEntity(int x, int y) const;
	void SetTileEntity(const TileEntity& entity, int x, int y);

	float GetTileHeight(const nsTile::Tile tile) const;
	float GetTileEntityHeight(const TileEntity& entity, const vector2df pos = vector2df(0.5f, 0.5f)) const;

	bool IsEntityInteractable(const nsTileEntity::TileEntity entity) const;
	const MeshBase* GetEntityMesh(const nsTileEntity::TileEntity entity) const;
	vector3df GetEntityOffset(const TileEntity& entity) const;
	
	const std::vector<vector2di>& GetSpawnPoints() const { return m_SpawnPoints; }

protected:

private:
	SceneManager*					m_pSceneMan;
	const GLTexture*				m_Texture;
	TerrainChunkMesh				m_TileMesh;
	std::vector<MeshBase*>			m_Meshes;
	EntityMeshArray					m_EntityMeshes;
	vector2di						m_CurrentChunk;
	vector2di						m_PreviousChunk;

	std::vector<Area>				m_Areas;
	std::vector<vector2di>			m_SpawnPoints;
	int								m_iCurrentArea;
	int								m_iCurrentLevel;

	void CleanUp();
	void AddMesh(const MeshBase* mesh, const vector3df& offset, float rot = 0);
	Mesh<GLVertex>* GetMesh(const vector3df& pos);
	Mesh<GLVertex>* GetMesh(int x, int y);
	TileEntity& _GetTileEntity(const vector2di& pos);
};

#endif

#ifndef GL_BASE_H_
#define GL_BASE_H_

// Include file for classes which need access to OpenGL
#define GLEW_STATIC
#define GLFW_INCLUDE_GL_3
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Vector.h"

#define VALID_BUFFER(buf) buf > 0

// GLVertex structure
// The structure is as follows (in bytes):
//  0  0  0  0  0  0  0  0  0  0  1  1  1  1  1  1  1  1  1  1  2  2  2  2  2  2  2  2  2  2  3  3 
//  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5  6  7  8  9  0  1 
// [ X        ][ Y        ][ Z        ][ R G B A  ][ UV       ][ Padding                          ]
#pragma pack(1)
struct GLColour {
	GLubyte r;
	GLubyte g;
	GLubyte b;
	GLubyte a;

	GLColour() { r = 255; g = 255; b = 255; a = 255; }
	GLColour(GLubyte _r, GLubyte _g, GLubyte _b, GLubyte _a) { Set(_r, _g, _b, _a); }
	GLColour(GLubyte _c, GLubyte _a) { Set(_c, _c, _c, _a); }
	GLColour(GLubyte _c) { Set(_c, _c, _c, 255); }
	GLColour(const GLColour& o) { Set(o); }
	void Set(GLubyte _r, GLubyte _g, GLubyte _b, GLubyte _a) { r = _r; g = _g; b = _b; a = _a; }
	void Set(const GLColour& o) { Set(o.r, o.g, o.b, o.a); }

	void operator=(const GLColour& o) { Set(o); }
	void operator*=(GLfloat v) { r = static_cast<GLubyte>(r * v); g = static_cast<GLubyte>(g * v); b = static_cast<GLubyte>(b * v); }
	void operator/=(GLfloat v) { r = static_cast<GLubyte>(r / v); g = static_cast<GLubyte>(g / v); b = static_cast<GLubyte>(b / v); }
	GLColour operator+(const GLColour& o) const { return GLColour(r + o.r, g + o.g, b + o.g, a + o.a); }
	GLColour operator*(GLfloat v) const { return GLColour(static_cast<GLubyte>(r * v), static_cast<GLubyte>(g * v), static_cast<GLubyte>(b * v), 255); }
	const bool operator==(const GLColour& o) const { return (r == o.r && g == o.g && b == o.b && a == o.a); }
	const bool operator!=(const GLColour& o) const { return !operator==(o); }
};

struct GLTexCoord {
	GLshort u;
	GLshort v;

	GLTexCoord() { u = 0; v = 0; }
	GLTexCoord(const vector2df& other) { u = static_cast<GLshort>(other.x * 8192); v = static_cast<GLshort>(other.y * 8192); }
	void operator=(const GLTexCoord& other) { u = other.u; v = other.v; }
	vector2df ToVector() { return vector2df((GLfloat)u / 8192.f, (GLfloat)v / 8192.f); }
	void Set(const vector2df& other) { u = static_cast<GLshort>(other.x * 8192); v = static_cast<GLshort>(other.y * 8192); }
	void Set(float x, float y) { u = static_cast<GLshort>(x * 8192); v = static_cast<GLshort>(y * 8192); }
};

struct GLVertex {
	vector3df	pos;
	GLColour	col;
	GLTexCoord	uv;
	GLfloat		padding[3];

	GLVertex(const vector3df& _pos, const GLColour& _col, const GLTexCoord& _uv) { pos = _pos; col = _col; uv = _uv; }
	GLVertex(const GLVertex& other) { pos = other.pos; col = other.col; uv = other.uv; }
};

// GLVertex2D structure
// The structure is as follows (in bytes):
//  0  0  0  0  0  0  0  0  0  0  1  1  1  1  1  1 
//  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5 
// [ X        ][ Y        ][ R G B A  ][ UV       ]
struct GLVertex2D {
	vector2df	pos;
	GLColour	col;
	GLTexCoord	uv;

	GLVertex2D() : pos(), col(), uv() {}
	GLVertex2D(const vector2df& _pos, const GLColour& _col, const GLTexCoord& _uv = GLTexCoord()) { pos = _pos; col = _col; uv = _uv; }
	GLVertex2D(const GLVertex2D& other) { pos = other.pos; col = other.col; uv = other.uv; }
};
#pragma pack()

#endif

#ifndef LIGHT_MANAGER_H
#define LIGHT_MANAGER_H

#include "GLShaderProgram.h"
#include "Mesh.h"
#include <vector>
#include <memory>

// Light struct - replaces the Light class (deleted) as it is no longer needed
struct Light {
	vector2df position;
	vector2df prev_position;
	GLColour colour;
	vector2df size;
	vector2df prev_size;
	bool fading;
	bool alive;
	bool reserved; // This allows us to keep the light index after fading out / killing a light

	Light() : position(), prev_position(), colour(), size(), prev_size(), fading(false), alive(false), reserved(false) {}
	Light(const Light& o)
		: position(o.position)
		, prev_position(o.prev_position)
		, colour(o.colour)
		, size(o.size)
		, prev_size(o.prev_size)
		, fading(o.fading)
		, alive(o.alive)
		, reserved(o.reserved)
	{}
};

// LightManager class - handles the data behind the lightmap

class LightManager {
public:
	LightManager(int size, int resolution);
	~LightManager();

	Light* CreateLight(const vector2df& pos, const GLColour& col, const vector2df& size, bool reserved = false);

	void Update();
	void ThreadUpdate();
	void Render();

	unsigned int GetLightMap() const { return m_LightMap; }

protected:

private:
	int									m_Size;
	int									m_NextIndex;
	int									m_MaxLight;
	int									m_Resolution;
	Light*								m_Lights;
	bool								m_bUpdated;
	size_t								m_RenderSize;

	std::unique_ptr<GLShaderProgram>	m_Shader;
	unsigned int						m_Texture;
	unsigned int						m_LightFBO;
	unsigned int						m_LightMap;
	int									m_LightResolution;

	int									m_PositionAttrib;
	int									m_ColourAttrib;
	int									m_TextureAttrib;

	std::unique_ptr<Mesh<GLVertex2D>>	m_Mesh;

	int GetNextIndex();
};

#endif // LIGHT_MANAGER_H
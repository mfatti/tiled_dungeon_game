#include "StatusEffect.h"
#include "StatusEffectImplementations.h"
#include "StatusEffectManager.h"
#include "SceneManager.h"
#include "Unit.h"

StatusEffect::StatusEffect(SceneManager* sceneman, Unit* owner, Unit* target, const std::string& name, int life, int rounds, float modifier, nsStatusEffect::EffectType type)
	: m_SceneMan(sceneman)
	, m_Owner(owner)
	, m_Target(target)
	, m_Life(life)
	, m_RoundsLeft(rounds)
	, m_Modifier(modifier)
	, m_TargetEmitter(nullptr)
	, m_OwnerEmitter(nullptr)
	, m_Name(name)
	, m_Type(type)
{
	// Create our particle effects if we need to - only create them if this status effect isn't active on the unit already
	const std::vector<ParticleDefinition>& defs = ParticleDefinitionLibrary::GetDefinitions(m_Name);
	if (m_Target && defs.size() > 0 && !Singleton<StatusEffectManager>::Instance().StatusEffectAlreadyApplied(m_Target, GetType()))
	{
		// First definition found will apply on the target
		m_TargetEmitter = m_SceneMan->CreateParticleEmitter(m_Target->GetPosition(), defs.at(0));
	}
	
	if (m_Owner && defs.size() > 1 && !Singleton<StatusEffectManager>::Instance().StatusEffectAlreadyApplied(m_Owner, GetType(), true))
	{
		// Second definition found will apply on the owner
		m_OwnerEmitter = m_SceneMan->CreateParticleEmitter(m_Owner->GetPosition(), defs.at(1));
	}
}

StatusEffect::~StatusEffect()
{
	if (m_TargetEmitter)
		m_TargetEmitter->Stop();
	
	if (m_OwnerEmitter)
		m_OwnerEmitter->Stop();
}

bool StatusEffect::TurnStart()
{
	// At the start of the turn we need to see if we are now dead
	// Note that although this can be overriden by staus effects, it should be called by all
	return --m_RoundsLeft == 0;
}

void StatusEffect::UpdatePosition()
{
	// Move our particle effects
	if (m_TargetEmitter)
		m_TargetEmitter->Translate(m_Target->GetPosition() - m_TargetEmitter->GetPosition());
	
	if (m_OwnerEmitter)
		m_OwnerEmitter->Translate(m_Owner->GetPosition() - m_OwnerEmitter->GetPosition());
}

StatusEffect* StatusEffect::CreateStatusEffect(nsStatusEffect::EffectType type, SceneManager* sceneman, Unit* owner, Unit* target, int life, int rounds, float modifier)
{
	StatusEffect* effect = nullptr;
	
	// Create a new status effect based on the type
	switch (type)
	{
		default:
		case nsStatusEffect::none:
			// Don't create anything
			break;
		case nsStatusEffect::fire:
			
			break;
		case nsStatusEffect::ice:
			
			break;
		case nsStatusEffect::poison:
			
			break;
		case nsStatusEffect::life_steal:
			
			break;
		case nsStatusEffect::life_transfer:
			
			break;
		case nsStatusEffect::heal:
			
			break;
		case nsStatusEffect::shield:
			effect = new StatusEffect_Shield(sceneman, owner, target, "Shield", life, rounds, modifier);
			break;
		case nsStatusEffect::damage_transfer:
			effect = new StatusEffect_DamageTransfer(sceneman, owner, target, "Damage Transfer", life, rounds, modifier);
			break;
		case nsStatusEffect::buff_damage:
			
			break;
		case nsStatusEffect::buff_health:
			
			break;
		case nsStatusEffect::buff_movement:
			
			break;
	}
	
	return effect;
}
#ifndef VIEW_FRUSTUM_H_
#define VIEW_FRUSTUM_H_

#include "Matrix.h"
#include "Plane.h"

class ViewFrustum {
public:
	enum {
		far_plane = 0,
		near_plane,
		left_plane,
		right_plane,
		bottom_plane,
		top_plane,

		plane_count
	};

	ViewFrustum();

	void Set(matrix4f& matrix);

	vector3df FarLeftUp() const;
	vector3df FarLeftDown() const;
	vector3df FarRightUp() const;
	vector3df FarRightDown() const;

	bool PointInFrustum(const vector3df& p) const;
	bool QuickPointInFrustum(const vector3df& p, float radius) const;

protected:

private:
	plane3df m_Planes[plane_count];
};

#endif
#ifndef PLAYER_CONTROLLER_H_
#define PLAYER_CONTROLLER_H_

#include "Plane.h"
#include "UnitController.h"

namespace nsPlayerMode
{
	enum PlayerMode {
		movement = 0,
		ability_1,
		ability_2,
		ability_3,
		ability_4,
		special
	};
}

// PlayerController class - handles control of player characters, ie movement, camera movement, actions etc

class PlayerController : public UnitController {
public:
	PlayerController(SceneManager* sceneman, UnitControllerManager* man);

	virtual void Update();
	virtual void UpdateBackground();
	virtual void TurnStart();
	virtual void TurnEnd();

	virtual bool MouseEvent(int button, int action, int mods);
	virtual bool KeyboardEvent(int key, int scancode, int action, int mods);

	int GetMovementLength() const { return m_MovementPath.size() - 1; }

	std::string GetMouseText();

	size_t GetCurrentCharacter() const { return m_CurrentCharacter; }
	virtual void GetUnitCounts(size_t id, int& act, int& max_act, int& step) const;
	void GetUnitHealth(size_t id, int& health, int& max_health) const;
	nsPlayerMode::PlayerMode GetMode() const { return m_Mode; }

	void EnemyKilled(const vector2di& pos);

	virtual bool DamageUnit(Unit* unit, int damage, Unit* other = nullptr);
	virtual bool HealUnit(Unit* unit, int heal, Unit* other = nullptr);

protected:

private:
	typedef std::unordered_map<size_t, Light*> LightMap;

	size_t						m_CurrentCharacter;
	size_t						m_CharacterCount;
	nsPlayerMode::PlayerMode	m_Mode;
	LightMap					m_Lights;
	SceneNode*					m_ControlNode;
	SceneNode*					m_GridNode;
	float						m_Animation;
	float						m_AnimationDirection;
	vector3df					m_MousePosition;
	vector2di					m_MouseGridPosition;
	vector2di					m_PreviousGridPosition;
	vector2di					m_PreviousCharacterPosition;
	int							m_PreviousAbility;
	float						m_TileHeight;
	plane3df					m_CollisionPlane;
	std::vector<vector2di>		m_MovementPath;
	std::vector<vector2di>		m_AbilityArea;
	bool						m_ValidMovement;
	std::vector<UnitData>		m_UnitData;
	bool						m_Attacking;
	
	void UpdateCamera();
	void UpdateIndicators();
	void UpdateMovement();
	void UpdateAbility();
	
	bool LeftClick();
	
	int GetAbilityIndex() const { return static_cast<int>(m_Mode - nsPlayerMode::ability_1); }
	
	void UpdateLighting();
	Light* GetUnitLight(Unit* unit) { return m_Lights.at(unit->GetID()); }
	Light* GetUnitLight(size_t id) { return m_Lights.at(id); }
	
	bool ChooseNextCharacter();
};

#endif // PLAYER_CONTROLLER_H_

#ifndef DATA_FILE_H
#define DATA_FILE_H

#include "GLBase.h"
#include <fstream>

namespace nsDataFileRead
{
	enum DataFileRead {
		empty = 0,
		section,
		attribute,
		end_of_file
	};
}

// DataFile class - handles reading a data file for unit data, attack data etc as they all use CSV files

class DataFile {
public:
	DataFile(const std::string& filename);
	~DataFile() { if (m_File.is_open()) m_File.close(); }

	bool IsOpen() const { return m_File.is_open(); }

	nsDataFileRead::DataFileRead Read();

	const std::string& ReadString() const { return m_Value; }
	bool ReadBool() const;
	int ReadInt() const;
	float ReadFloat() const;
	template<class T> Vector2<T> ReadVector2() const;
	template<class T> Vector3<T> ReadVector3() const;
	GLColour ReadGLColour() const;

	const std::string& GetAttributeName() const { return m_AttributeName; }
	int GetDepth() const { return m_Depth; }

protected:

private:
	std::ifstream	m_File;
	std::string		m_Value;
	std::string		m_AttributeName;
	size_t			m_LinePos;
	int				m_Depth;
	
	bool ReadToSeparator(char separator, size_t& pos, std::string& out) const;
};

#endif // DATA_FILE_H
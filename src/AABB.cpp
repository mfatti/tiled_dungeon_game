#include "AABB.h"
#include "ViewFrustum.h"

// AABB class

void AABB::SetAABB(const vector3df& bl, const vector3df& tr)
{
	bottomLeft.Set(bl);
	topRight.Set(tr);
	midPoint = (topRight + bottomLeft) * 0.5f;

	size = bottomLeft.Distance(topRight);
	halfsize = size * 0.5f;
}

bool AABB::InViewFrustum(const ViewFrustum& frustum) const
{
	// check bottom left
	if (frustum.PointInFrustum(bottomLeft))
		return true;

	// check top right
	if (frustum.PointInFrustum(topRight))
		return true;

	// check other points
	if (frustum.PointInFrustum(vector3df(bottomLeft.x, bottomLeft.y, topRight.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(bottomLeft.x, topRight.y, topRight.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(topRight.x, bottomLeft.y, topRight.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(topRight.x, bottomLeft.y, bottomLeft.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(bottomLeft.x, topRight.y, bottomLeft.z)))
		return true;
	if (frustum.PointInFrustum(vector3df(topRight.x, topRight.y, bottomLeft.z)))
		return true;

	// we are not in the frustum
	return false;
}

bool AABB::QuickInViewFrustum(const ViewFrustum& frustum, const vector3df& pos) const
{
	// check the middle point with the halfsize
	return frustum.QuickPointInFrustum(midPoint + pos, halfsize);
}

// AABB2D class

void AABB2D::SetAABB(const vector2df& br, const vector2df& tl)
{
	m_BottomRight = br;
	m_TopLeft = tl;
}

void AABB2D::SetOffset(const vector2df& offset)
{
	m_Offset = offset;
}

bool AABB2D::PointInside(const vector2df& pos) const
{
	return (pos.x >= m_Offset.x + m_BottomRight.x && pos.x <= m_Offset.x + m_TopLeft.x && pos.y >= m_Offset.y + m_BottomRight.y && pos.y <= m_Offset.y + m_TopLeft.y);
}
#ifndef CAMERA_H_
#define CAMERA_H_

#include "ViewFrustum.h"

class Camera {
public:
	Camera(const matrix4f& proj);
	~Camera();

	void LookAt(const vector3df& pos) { m_Target = pos; }
	void Translate(const vector3df& pos) { m_Position += pos; }
	void SetPosition(const vector3df& pos) { m_Position = pos; }
	void SetRotation(const vector3df& rot) { m_Rotation = rot; }

	const vector3df& GetPosition() const { return m_Position; }

	void SetBillboardPosition(const vector3df& pos) { m_BillboardPosition = pos; }
	const vector3df& GetBillboardPosition() const { return m_BillboardPosition; }

	void Update();
	const float* GetCameraMatrix() const { return m_Matrix.GetDataPointer(); }
	const matrix4f& GetMatrix() const { return m_Matrix; }

	const ViewFrustum& GetViewFrustum() const { return m_Frustum; }

	const float& GetDirection() const { return m_fDirection; }
	const float& GetDirectionCos() const { return m_fDirCos; }
	const float& GetDirectionSin() const { return m_fDirSin; }

	const vector3df& GetTarget() const { return m_Target; }

protected:

private:
	matrix4f		m_Matrix;
	matrix4f		m_ProjMatrix;
	vector3df		m_Position;
	vector3df		m_BillboardPosition;
	vector3df		m_Target;
	ViewFrustum		m_Frustum;
	float			m_fDirection;
	float			m_fDirCos;
	float			m_fDirSin;
	vector3df		m_Rotation;
};

#endif

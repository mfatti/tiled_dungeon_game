#include "GUIManager.h"
#include <string>
#include <algorithm>

GUIManager::GUIManager()
	: m_TexMan([](const std::string& filename) { return new GLTexture(filename); })
	, m_FontMan([](const std::string& key) { return new Font(key); })
	, m_GUITexture(0u)
	, m_bLabel(false)
{
	REGISTER_INPUT_OBJECT;
	SetOverride();
}

void GUIManager::Initialise()
{
	// Create our GLSL shader programs
	std::string strVertGUI =
		"#version 130\n"
		"in vec2 position;\n"
		"in vec4 colour;\n"
		"in vec2 texcoord;\n"
		"out vec4 vertexColour;\n"
		"out vec2 textureCoord;\n"
		"uniform mat4 model;\n"
		"uniform mat4 projection;\n"
		"void main() {\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    vertexColour = colour;\n"
		"    gl_Position = projection * model * vec4(position, 0.0, 1.0);\n"
		"}";

	std::string strFragGUI =
		"#version 130\n"
		"in vec4 vertexColour;\n"
		"in vec2 textureCoord;\n"
		"uniform sampler2D texImage;\n"
		"out vec4 FragColour;\n"
		"void main() {\n"
		//"    if (gl_FragCoord.x < size.x || gl_FragCoord.x > size.z || gl_FragCoord.y < size.y || gl_FragCoord.y > size.w) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		//"    textureColour.a *= alpha;\n"
		"    if (textureColour.a < 0.01) discard;\n"
		"\n"
		"    FragColour = textureColour * vertexColour;\n"
		"}";

	// Add this shader program to our vector
	GLShaderProgram* program = new GLShaderProgram(strVertGUI, strFragGUI);
	program->SetOrthoProjection(GET_SCREEN_SIZE.x, GET_SCREEN_SIZE.y);

	program->Bind();
	// Get our attribute and uniform positions
	m_iPositionAttrib = program->GetAttribLocation("position");
	m_iColourAttrib = program->GetAttribLocation("colour");
	m_iTextureAttrib = program->GetAttribLocation("texcoord");
	m_iModelUniform = program->GetUniformLocation("model");
	m_iProjectionUniform = program->GetUniformLocation("projection");
	m_iTexImageUniform = program->GetUniformLocation("texImage");
	
	// Send default value in for the teximage uniform
	glUniform1i(m_iTexImageUniform, 0);
	//glUniform4f(SHADER_GUI_SIZE_UNIFORM, 0.f, 0.f, 1280.f, 720.f);
	//glUniform1f(SHADER_GUI_ALPHA_UNIFORM, 1.f);
	program->Unbind();

	// Set this as our program
	m_Shader.reset(program);

	// Load our default texture
	m_GUITexture = m_TexMan.GetResource("media/all_white.png").ID();

	// Create our GUILabel
	m_GUILabel.reset(new GUILabel(this));
}

void GUIManager::Update()
{
	// Set our label flag to false
	m_bLabel = false;

	// Update each GUI element individually
	GUIList::iterator it = m_GUIElements.begin();
	while (it != m_GUIElements.end())
	{
		// Check if this GUI element is dead, and if it is, delete it
		if ((*it)->IsDead())
		{
			const GLTexture* tex = (*it)->GetTexture();
			if (tex)
				GetMesh(tex->ID()).needs_building = true;

			it = m_GUIElements.erase(it);
		}
		else
		{
			(*it)->Update();

			// Specific GUI element handling
			if ((*it)->GetType() == GUIType::gui_textbox)
			{
				GUITextBox* textbox = dynamic_cast<GUITextBox*>((*it).get());
				if (IS_KEY_PRESSED(GLFW_KEY_BACKSPACE))
					textbox->EraseCharacter();
				else textbox->ResetEraseDelay();
			}

			++it;
		}
	}

	// Update each GUI text element individually
	GUITextList::iterator tit = m_GUITextElements.begin();
	while (tit != m_GUITextElements.end())
	{
		// Check if this GUI element is dead, and if it is, delete it
		if ((*tit)->IsDead())
			tit = m_GUITextElements.erase(tit);
		else
		{
			(*tit)->Update();
			++tit;
		}
	}
	
	// Rebuild our mesh if we need to
	MeshMap::iterator mit = m_Meshes.begin();
	while (mit != m_Meshes.end())
	{
		if (mit->second.needs_building)
		{
			mit->second.mesh->Clear();

			// Find all GUI elements and build the ones for this mesh
			for (auto& eit : m_GUIElements) {
				const GLTexture* tex = eit->GetTexture();

				if (tex && tex->ID() == mit->first)
				{
					eit->ConstructMesh();
				}
			}

			// Clear the flag
			mit->second.needs_building = false;
		}

		++mit;
	}

	// Update our GUILabel
	m_GUILabel->Update();

	if (!m_bLabel)
		m_GUILabel->SetTitle();
}

void GUIManager::Render()
{
	// Begin the rendering
	StartRender();

	// Render our meshes
	static const matrix4f m;
	glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, m.GetDataPointer());

	MeshMap::iterator it = m_Meshes.begin();
	while (it != m_Meshes.end())
	{
		// If we need to buffer the mesh, do so now
		if (it->second.mesh->IsDirty())
			it->second.mesh->Buffer();

		// If we have a valid buffer
		if (it->second.mesh->GetIndexBufferSize() > 0)
		{
			UseTexture(it->first);
			RenderMesh(it->second.mesh.get());
		}

		++it;
	}

	// Render our GUIElements
	for (auto& it : m_GUITextElements) {
		// Firstly render this element
		const MeshBase* mesh = it->GetMesh();
		if (mesh && it->IsVisible())
		{
			if (mesh->IsDirty())
				it->ConstructMesh();

			if (VALID_BUFFER(mesh->GetVBO()))
			{
				glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, it->GetTransformationMatrix().GetDataPointer());
				//glActiveTexture(GL_TEXTURE0);
				if (it->GetTexture())
					UseTexture(it->GetTexture()->ID());
				RenderMesh(mesh);
			}
		}
	}

	// Render our GUILabel
	if (m_GUILabel->IsVisible())
	{
		// Render the background
		const MeshBase* mesh = m_GUILabel->GetMesh();
		
		if (mesh->IsDirty())
			m_GUILabel->ConstructMesh();
		
		if (VALID_BUFFER(mesh->GetVBO()))
		{
			glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, m_GUILabel->GetTransformationMatrix().GetDataPointer());
			UseTexture(m_GUITexture);
			RenderMesh(mesh);
		}
		
		// Now the title
		mesh = m_GUILabel->GetTitle()->GetMesh();
		if (VALID_BUFFER(mesh->GetVBO()))
		{
			glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, m_GUILabel->GetTitle()->GetTransformationMatrix().GetDataPointer());
			if (m_GUILabel->GetTitle()->GetTexture())
				UseTexture(m_GUILabel->GetTitle()->GetTexture()->ID());
			RenderMesh(mesh);
		}
		
		// Now the description
		mesh = m_GUILabel->GetDescription()->GetMesh();
		if (VALID_BUFFER(mesh->GetVBO()))
		{
			glUniformMatrix4fv(m_iModelUniform, 1, GL_TRUE, m_GUILabel->GetDescription()->GetTransformationMatrix().GetDataPointer());
			if (m_GUILabel->GetDescription()->GetTexture())
				UseTexture(m_GUILabel->GetDescription()->GetTexture()->ID());
			RenderMesh(mesh);
		}
	}

	// Finish the rendering
	FinishRender();
}

void GUIManager::UseTexture(GLuint texture)
{
	// Set the texture currently needed for rendering
	if (m_CurrentActiveTexture != texture)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		m_CurrentActiveTexture = texture;
	}
}

const GLTexture& GUIManager::GetTexture(const std::string& filename)
{
	return m_TexMan.GetResource(filename);
}

const Font& GUIManager::GetFont(const std::string& key)
{
	return m_FontMan.GetResource(key);
}

GLuint GUIManager::AddQuad(const GUIElement* who, const GLVertex2D& ver1, const GLVertex2D& ver2, const GLVertex2D& ver3, const GLVertex2D& ver4, bool flip)
{
	// Find the mesh
	const GLTexture* tex = who->GetTexture();
	if (tex)
	{
		GUIMesh& mesh = GetMesh(tex->ID());
		return mesh.mesh->AddQuad(ver1, ver2, ver3, ver4, flip);
	}

	// The GUIElement doesn't have a texture
	return 0u;
}

void GUIManager::ChangeQuadColours(const GUIElement* who, GLuint pos, const GLColour& col1, const GLColour& col2, const GLColour& col3, const GLColour& col4)
{
	// Find the mesh
	const GLTexture* tex = who->GetTexture();
	if (tex)
	{
		GUIMesh& mesh = GetMesh(tex->ID());
		pos += who->GetQuadStart();
		pos *= 4;
		
		// Update the colours!
		std::vector<GLVertex2D>& data = mesh.mesh->GetVertexData();
		if (pos + 3 < data.size())
		{
			data.at(pos).col = col1;
			data.at(pos + 1).col = col2;
			data.at(pos + 2).col = col3;
			data.at(pos + 3).col = col4;

			// Set the mesh as dirty
			mesh.mesh->SetDirty();
		}
	}
}

void GUIManager::ChangeQuadPosition(const GUIElement* who, GLuint pos, const vector2df& position)
{
	// Find the mesh
	const GLTexture* tex = who->GetTexture();
	if (tex)
	{
		GUIMesh& mesh = GetMesh(tex->ID());
		pos += who->GetQuadStart();
		pos *= 4;
		
		// Update the colours!
		std::vector<GLVertex2D>& data = mesh.mesh->GetVertexData();
		if (pos + 3 < data.size())
		{
			// Get differences between quad positions
			vector2df abspos(position + who->GetAbsolutePosition());
			vector2df diff1(data.at(pos + 1).pos - data.at(pos).pos);
			vector2df diff2(data.at(pos + 2).pos - data.at(pos).pos);
			vector2df diff3(data.at(pos + 3).pos - data.at(pos).pos);

			data.at(pos).pos = abspos;
			data.at(pos + 1).pos = abspos + diff1;
			data.at(pos + 2).pos = abspos + diff2;
			data.at(pos + 3).pos = abspos + diff3;

			// Set the mesh as dirty
			mesh.mesh->SetDirty();
		}
	}
}

void GUIManager::ChangeQuadPositions(const GUIElement* who, GLuint pos, const vector2df& pos1, const vector2df& pos2, const vector2df& pos3, const vector2df& pos4)
{
	// Find the mesh
	const GLTexture* tex = who->GetTexture();
	if (tex)
	{
		GUIMesh& mesh = GetMesh(tex->ID());
		pos += who->GetQuadStart();
		pos *= 4;
		
		// Update the colours!
		std::vector<GLVertex2D>& data = mesh.mesh->GetVertexData();
		if (pos + 3 < data.size())
		{
			// Get differences between quad positions
			vector2df abspos(who->GetAbsolutePosition());

			data.at(pos).pos = abspos + pos1;
			data.at(pos + 1).pos = abspos + pos2;
			data.at(pos + 2).pos = abspos + pos3;
			data.at(pos + 3).pos = abspos + pos4;

			// Set the mesh as dirty
			mesh.mesh->SetDirty();
		}
	}
}

void GUIManager::MoveElement(const GUIElement* element, const vector2df& move)
{
	// Find the mesh
	const GLTexture* tex = element->GetTexture();
	if (tex)
	{
		GUIMesh& mesh = GetMesh(tex->ID());

		// Move all quads that this element (and its children) own
		unsigned int size = (element->GetQuadStart() + element->GetQuadCount()) * 4;
		std::vector<GLVertex2D>& data = mesh.mesh->GetVertexData();
		for (unsigned int i = element->GetQuadStart() * 4; i < size; ++i) {
			data.at(i).pos += move;
		}
		mesh.mesh->SetDirty();
	}
}

void GUIManager::RebuildMesh(const GUIElement* who)
{
	// Set our mesh to be rebuilt - this will happen on death of a GUI widget or if a widget goes invisible
	const GLTexture* tex = who->GetTexture();
	if (tex)
	{
		GUIMesh& mesh = GetMesh(tex->ID());
		mesh.needs_building = true;
	}
	for (auto& it : m_Meshes) {
		it.second.needs_building = true;
	}
}

void GUIManager::SetLabel(const std::string& title, const std::string& description)
{
	m_GUILabel->SetTitle(title);
	m_GUILabel->SetDescription(description);

	if (!title.empty())
		m_bLabel = true;
}

bool GUIManager::MouseEvent(int button, int action, int mods)
{
	switch (button)
	{
		case GLFW_MOUSE_BUTTON_1:
		{
			// Left click, pick up/put down/throw items from the inventory
			if (action == GLFW_PRESS)
			{
				// Loop through text boxes and handle focusing
				for (auto& it : m_GUIElements) {
					if (it->IsVisible())
					{
						switch (it->GetType())
						{
						case GUIType::gui_textbox:
						{
							GUITextBox* textbox = dynamic_cast<GUITextBox*>(it.get());
							if (textbox->IsHovered() && !textbox->IsFocused())
							{
								textbox->SetFocus();
								return true;
							}
							else if (!textbox->IsHovered() && textbox->IsFocused())
								textbox->SetFocus(false);
						}
						break;
						case GUIType::gui_button:
						{
							GUIButton* button = dynamic_cast<GUIButton*>(it.get());
							if (button->IsHovered() && button->OnClick())
							{
								return true;
							}
						}
						break;
						default:
							// Do nothing
							break;
						}
					}
				}
			}
		}
		break;
		default:
		break;
	}

	return false;
}

bool GUIManager::CharEvent(unsigned int codepoint)
{
	// Input into the focused textbox
	for (auto& it : m_GUIElements) {
		if (it->GetType() == GUIType::gui_textbox)
		{
			GUITextBox* textbox = dynamic_cast<GUITextBox*>(it.get());
			if (textbox->IsFocused())
			{
				textbox->CharInput(codepoint);
				return true;
			}
		}
	}

	return false;
}

void GUIManager::Sort()
{
	m_GUIElements.sort([](const std::unique_ptr<GUIElement>& one, const std::unique_ptr<GUIElement>& two) {
		return (*one < *two);
	});
}

void GUIManager::Reverse()
{
	if (!m_GUIElements.empty())
		std::reverse(m_GUIElements.begin(), m_GUIElements.end());
}

void GUIManager::EnableVertexAttributes()
{
	// Our shaders have been set up with fixed attribute locations, meaning we know exactly where everything lies
	// This is set up in the defines in the SceneManager header file
	glEnableVertexAttribArray(m_iPositionAttrib);
	glEnableVertexAttribArray(m_iColourAttrib);
	glEnableVertexAttribArray(m_iTextureAttrib);

	GLsizei size = sizeof(GLVertex2D);
	glVertexAttribPointer(m_iPositionAttrib, 2, GL_FLOAT, GL_FALSE, size, 0);
	glVertexAttribPointer(m_iColourAttrib, 4, GL_UNSIGNED_BYTE, GL_TRUE, size, (GLvoid *)(8));
	glVertexAttribPointer(m_iTextureAttrib, 2, GL_SHORT, GL_FALSE, size, (GLvoid *)(12));
}

void GUIManager::DisableVertexAttributes()
{
	// Disable the vertex attributes
	glDisableVertexAttribArray(m_iPositionAttrib);
	glDisableVertexAttribArray(m_iColourAttrib);
	glDisableVertexAttribArray(m_iTextureAttrib);
}

void GUIManager::StartRender()
{
	// Clear the depth buffer and disable culling
	glClear(GL_DEPTH_BUFFER_BIT);
	glDisable(GL_CULL_FACE);

	// Reset the current active texture as this could be wrong due to outside events
	m_CurrentActiveTexture = 0u;

	// Bind our shader program
	m_Shader->Bind();
}

void GUIManager::FinishRender()
{
	// Re-enable culling
	glEnable(GL_CULL_FACE);
}

void GUIManager::RenderMesh(const MeshBase* mesh)
{
	// Render this mesh
	glBindBuffer(GL_ARRAY_BUFFER, mesh->GetVBO());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetEBO());
	EnableVertexAttributes();
	glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh->GetIndexBufferSize()), GL_UNSIGNED_INT, 0);
	DisableVertexAttributes();
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GUIManager::AddElement(GUIElement* element)
{
	// Add this GUI element either to our list or its parent's list
	m_GUIElements.push_back(std::unique_ptr<GUIElement>(element));
	Sort();

	// Build the element
	element->ConstructMesh();
}

GUIMesh& GUIManager::GetMesh(GLuint tex)
{
	// Find the mesh in our map
	MeshMap::iterator it = m_Meshes.find(tex);
	if (it != m_Meshes.end())
		return it->second;

	// Our mesh isn't yet created
	m_Meshes.emplace(tex, GUIMesh());
	return (m_Meshes.at(tex));
}

// GUI creation methods
GUIPane* GUIManager::CreatePane(const vector2df& pos, const vector2df& size, GUIElement* parent)
{
	GUIPane* pane = new GUIPane(this, size, parent);
	pane->SetPosition(pos);

	AddElement(pane);
	return pane;
}

GUIText* GUIManager::CreateText(const vector2df& pos, const std::string& text, const std::string& font, GUIElement* parent)
{
	GUIText* t = new GUIText(this, text, parent);
	t->SetFont(&GetFont(font));
	t->SetPosition(pos);
	t->ConstructMesh();

	m_GUITextElements.push_back(std::unique_ptr<GUIText>(t));
	return t;
}

GUIImage* GUIManager::CreateImage(const std::string& texture, const vector2df& position, const vector2df& size, GUIElement* parent)
{
	GUIImage* image = new GUIImage(this, texture, size, parent);
	image->SetPosition(position);
	image->ConstructMesh();

	AddElement(image);
	return image;
}

GUITextBox* GUIManager::CreateTextBox(const vector2df& pos, const vector2df& size, const std::string& font, GUIElement* parent)
{
	GUITextBox* textbox = new GUITextBox(this, font, size, parent);
	textbox->SetPosition(pos);

	AddElement(textbox);
	return textbox;
}

GUIButton* GUIManager::CreateButton(const vector2df& pos, const vector2df& size, const std::string& text, const std::string& font, GUIElement* parent)
{
	GUIButton* button = new GUIButton(this, text, font, size, parent);
	button->SetPosition(pos);

	AddElement(button);
	return button;
}

GUICharacterPane* GUIManager::CreateCharacterPane(const vector2df& pos, const std::string& text, GUIElement* parent)
{
	GUICharacterPane* pane = new GUICharacterPane(this, text, parent);
	pane->SetPosition(pos);

	AddElement(pane);
	return pane;
}

GUIAction* GUIManager::CreateAction(const vector2df& pos, GUIElement* parent)
{
	GUIAction* action = new GUIAction(this, parent);
	action->SetPosition(pos);
	
	AddElement(action);
	return action;
}

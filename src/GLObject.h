#ifndef GL_OBJECT_H_
#define GL_OBJECT_H_

#include "GLBase.h"
#include <vector>

// The base GLObject class - the purpose of this class is to provide a VBO/EBO for rendering

class GLObjectBase {
public:
	virtual GLuint GetVBO() const = 0;
	virtual GLuint GetEBO() const = 0;

	virtual ~GLObjectBase() {};

protected:

private:

};

template<typename T>
class GLObject : public GLObjectBase {
public:
	GLObject();
	virtual ~GLObject();

	bool BufferData(const std::vector<T>& VBO, const std::vector<GLuint>& EBO);
	bool UpdateVBO(const std::vector<T>& VBO, size_t size = 0);

	virtual GLuint GetVBO() const;
	virtual GLuint GetEBO() const;

protected:

private:
	GLuint	m_bufVBO;
	GLuint	m_bufEBO;

	void ClearBuffers();
};

#endif

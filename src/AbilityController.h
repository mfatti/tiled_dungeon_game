#ifndef ABILITY_CONTROLLER_H
#define ABILITY_CONTROLLER_H

#include "Unit.h"
#include "PathFinder.h"
#include "ParticleEmitter.h"
#include "Projectile.h"
#include "StatusEffect.h"
#include <map>
#include <vector>

class UnitController;

namespace nsAbilityCategory
{
	enum AbilityCategory {
		attack = 0,
		support,
		summon,
		special_attack,
		special_support,
		special_summon
	};
}

namespace nsAbilityType
{
	enum AbilityType {
		single_target = 0,
		aoe_target,
		aoe_anywhere,
		lance,
		self,
		self_aoe,
		invalid = 120
	};
}

struct Ability {
	short range = {0};
	short area = {0};
	short cost = {9999};
	nsAbilityType::AbilityType type = {nsAbilityType::invalid};
	nsAbilityCategory::AbilityCategory category = {nsAbilityCategory::attack};
	nsStatusEffect::EffectType status_effect = {nsStatusEffect::none};
	float effect_modifier = {0.f};
	short effect_life = {0};
	short round_count = {0};
	short cooldown = {1};
	bool affects_self = {false};
};

// AbilityController class - handles units using abilities to attack / support

class AbilityController {
public:
	AbilityController(UnitController* owner);

	void SetTargetController(UnitController* target) { m_Target = target; }

	void Update();

	bool UseAbility(const std::string& name, Unit* user, const vector2di& pos, int value);

	const Projectile* GetFirstProjectile() const;

	static bool LoadAbilities(const std::string& filename);
	static Ability& GetAbility(const std::string& name);

protected:

private:
	typedef std::map<std::string, Ability> AbilityMap;
	static AbilityMap	m_Abilities;

	UnitController*		m_Owner;
	UnitController*		m_Target;
	PathFinder			m_PathFinder;

	typedef std::vector<std::unique_ptr<Projectile>> ProjectileVector;
	ProjectileVector	m_Projectiles;

	bool PerformAbilityAction(const Ability& ability, const vector3df& target, int value, Unit* user);
};

#endif // ABILITY_CONTROLLER_H
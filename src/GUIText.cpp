#include "GUIText.h"
#include "GUIManager.h"
#include <sstream>
#include <algorithm>

GUIText::GUIText(GUIManager* guiMan, const std::string& text, GUIElement* parent)
	: GUIElement(guiMan, parent)
	, m_strText(text)
	, m_bSplitByWord(false)
	, m_TextAlignment(TextAlignment::left)
	, m_fLineSpacing(1.f)
	, m_LineHeight(1)
	, m_Mesh(new Mesh<GLVertex2D>)
	, m_bShadowed(false)
	, m_ShadowOffset(2.f, 3.f)
	, m_ucShadowAlpha(180)
{
	// Set our type
	SetType(GUIType::gui_text);

	// Set our default size
	SetSize(vector2df(320.f, 0.f));
}

void GUIText::Update()
{
	if (HasUpdated())
	{
		// Construct our transformation matrix
		m_Transformation.Identity();
		m_Transformation.Scale(GetScale());
		m_Transformation.SetTranslation(GetAbsolutePosition());
	}

	// Call base class Update
	GUIElement::Update();
}

void GUIText::SetFont(const Font* font)
{
	// Set the font
	m_Font = font;

	// If we have a valid font we'll want to update our own texture
	if (font)
		SetTexture(font->GetTexture());
}

void GUIText::SetText(const std::string& text)
{
	// If we are changing text, set us as dirty
	if (m_strText != text)
		m_Mesh->SetDirty();

	// Set our text
	m_strText = text;
}

void GUIText::AppendText(const std::string& text, int pos)
{
	// Mark us as dirty
	m_Mesh->SetDirty();
	
	// Append or insert text
	if (pos == -1)
		m_strText.append(text);
	else
		m_strText.insert(static_cast<size_t>(pos), text);
}

const std::string& GUIText::GetText() const
{
	return m_strText;
}

const std::string GUIText::GetReadableText() const
{
	// Get a readable version of the text
	size_t length = GetTextLength();
	char c = m_strText[0];
	GLColour col;
	std::stringstream str;

	for (size_t i = 0; i < length; c = m_strText[++i]) {
		// Sort out the colour
		if (c == '^' && m_strText[i + 1] != '\0')
		{
			if (GetColour(col, m_strText[i + 1]))
			{
				++i;
				continue;
			}
		}

		// Positional corrections inside text
		if (c == '%' && m_strText[i + 1] != '\0')
		{
			size_t j = i + 1;
			while (m_strText[j] != '\0')
			{
				if (m_strText[j] == '\\')
				{
					++j;
					break;
				}

				if (!isdigit(m_strText[j]))
					break;

				++j;
			}

			i += (j - i - 1);
			continue;
		}

		// Newlines
		if (c == '\n')
		{
			continue;
		}

		// Append the character onto our string
		str << c;
	}

	// Return the string
	return str.str();
}

size_t GUIText::GetTextLength() const
{
	return m_strText.length();
}

float GUIText::GetTextWidth() const
{
	if (!m_Font)
		return 0.f;

	return static_cast<float>(m_Font->GetStringWidth(GetReadableText()));
}

float GUIText::GetTextHeight() const
{
	if (!m_Font)
		return 0.f;

	std::vector<int> temp;
	GetLineLengths(temp);

	return static_cast<float>(temp.size() * m_Font->GetHeight() * 1.25f);
}

void GUIText::SetLineSpacing(float spacing)
{
	m_fLineSpacing = spacing;
}

float GUIText::GetLineSpacing() const
{
	return m_fLineSpacing;
}

void GUIText::SetAlignment(TextAlignment align)
{
	m_TextAlignment = align;

	m_Mesh->SetDirty();
}

void GUIText::SplitByWord(bool split)
{
	m_bSplitByWord = split;

	m_Mesh->SetDirty();
}

void GUIText::SetShadowed(bool shadow)
{
	m_bShadowed = shadow;

	m_Mesh->SetDirty();
}

const Mesh<GLVertex2D>* GUIText::GetMesh() const
{
	return m_Mesh.get();
}

const matrix4f& GUIText::GetTransformationMatrix() const
{
	return m_Transformation;
}

void GUIText::BuildElement()
{
	// Clear our buffer
	m_Mesh->Clear();
	
	// test
	/*m_Mesh->AddQuad(GLVertex2D(vector2df(), GLColour(255, 255), vector2df()),
		GLVertex2D(vector2df(512.f, 0.f), GLColour(255, 255), vector2df(1.f, 0.f)),
		GLVertex2D(vector2df(512.f, 512.f), GLColour(255, 255), vector2df(1.f, 1.f)),
		GLVertex2D(vector2df(0.f, 512.f), GLColour(255, 255), vector2df(0.f, 1.f)));
	
	// Buffer our data
	if (m_Mesh->IsDirty())
		m_Mesh->Buffer();
		
	return;*/
	
	// Only do this if we have a font and some text
	if (m_Font && !m_strText.empty())
	{
		// Variables for building the buffers
		char c = m_strText[0];
		int n = 0, t = 0;
		float y = 0;
		GLColour col, next_col;
		int width = static_cast<int>(GetSize().x);
		std::stringstream word;
		float height = static_cast<float>(m_Font->GetHeight());
		m_LineHeight = 1;
		std::vector<int> lineLengths;
		GetLineLengths(lineLengths);

		// Function to add a quad
		auto add_quad = [&](const FontGlyph& glyph) {
			vector2di size(GetTexture()->Dimensions());

			vector2df p1(static_cast<float>(n), floor(y * height));
			vector2df p2(static_cast<float>(n + glyph.advance), floor((y + 1.f) * height));
			vector2df uv1(static_cast<float>(glyph.pos.x) / static_cast<float>(size.x), static_cast<float>(glyph.pos.y) / static_cast<float>(size.y));
			vector2df uv2(static_cast<float>(glyph.pos.x + glyph.advance) / static_cast<float>(size.x), static_cast<float>(glyph.pos.y + height) / static_cast<float>(size.y));

			// Add a shadow if we need to
			if (m_bShadowed)
				m_Mesh->AddQuad(GLVertex2D(vector2df(p1.x, p1.y) + m_ShadowOffset, GLColour(0, m_ucShadowAlpha), vector2df(uv1.x, uv1.y)),
					GLVertex2D(vector2df(p2.x, p1.y) + m_ShadowOffset, GLColour(0, m_ucShadowAlpha), vector2df(uv2.x, uv1.y)),
					GLVertex2D(vector2df(p2.x, p2.y) + m_ShadowOffset, GLColour(0, m_ucShadowAlpha), vector2df(uv2.x, uv2.y)),
					GLVertex2D(vector2df(p1.x, p2.y) + m_ShadowOffset, GLColour(0, m_ucShadowAlpha), vector2df(uv1.x, uv2.y)));

			m_Mesh->AddQuad(GLVertex2D(vector2df(p1.x, p1.y), col, vector2df(uv1.x, uv1.y)),
				GLVertex2D(vector2df(p2.x, p1.y), col, vector2df(uv2.x, uv1.y)),
				GLVertex2D(vector2df(p2.x, p2.y), col, vector2df(uv2.x, uv2.y)),
				GLVertex2D(vector2df(p1.x, p2.y), col, vector2df(uv1.x, uv2.y)));
		};

		// Function to set alignment
		auto set_alignment = [&]() {
			size_t _where = std::min(lineLengths.size() - 1, m_LineHeight - 1);

			if (m_TextAlignment == TextAlignment::centre)
			{
				n = (width / 2) - (lineLengths[_where] / 2);
			}
			else if (m_TextAlignment == TextAlignment::right)
			{
				n = width - lineLengths[_where];
			}
		};
		set_alignment();

		// Is this the end of the current word?
		bool bEndOfWord(false);

		for (GLuint i = 0; c != '\0' && i < m_strText.length(); c = m_strText[++i]) {
			// Get the character details
			const FontGlyph& g = m_Font->GetCharacterInfo(c);

			// Sort out the colour
			bool bColourChange(false);
			if (c == '^' && m_strText[i + 1] != '\0')
			{
				bColourChange = GetColour(next_col, m_strText[i + 1]);
				if (bColourChange)
				{
					++i;
					
					// If we are splitting by words, end the word here
					if (m_bSplitByWord)
						bEndOfWord = true;
					else
						continue;
				}
			}
			
			// If there's no colour change set our colour
			if (!bColourChange)
				col = next_col;

			// Positional corrections inside text
			if ((!m_bSplitByWord || !bEndOfWord) && c == '%' && m_strText[i + 1] != '\0')
			{
				char a = m_strText[i + 1];
				if (isdigit(a))
				{
					std::stringstream s;
					int j = i + 1;

					while (m_strText[j] != '\0')
					{
						if (m_strText[j] == '\\')
						{
							++j;
							break;
						}

						if (!isdigit(m_strText[j]))
							break;

						s << m_strText[j];
						++j;
					}

					n = stoi(s.str());
					i += (j - i - 1);
					continue;
				}
			}

			// Add the character to our current word
			if (!bColourChange && c != '\n')
				word << c;

			// If we are splitting by word then add the whole word now
			bEndOfWord |= (c == ' ' || m_strText[i + 1] == '\0');
			if (bEndOfWord && m_bSplitByWord)
			{
				for (GLuint j = 0; j < word.str().length(); ++j) {
					// Get the glyph for the current character
					const FontGlyph& _g = m_Font->GetCharacterInfo(word.str()[j]);

					// Add a quad
					add_quad(_g);

					// Move on
					n += _g.advance;
				}

				t = n;
				word.str(std::string());
				word.clear();
				bEndOfWord = false;
			}

			// Newlines
			if (c == '\n' || t + g.advance > width)
			{
				// Move onto the next line
				y += m_fLineSpacing;
				++m_LineHeight;
				n = 0;
				t = 0;

				// Set alignment for the new line
				set_alignment();

				if (c == '\n' || (c == ' ' && !m_bSplitByWord))
				{
					if (i + 1 >= m_strText.length())
						break;

					continue;
				}
			}

			// Add the quad
			if (!m_bSplitByWord)
			{
				// Add a quad
				add_quad(g);

				// go to the next character
				n += g.advance;
			}

			t += g.advance;
		}
	}
	else
	{
		// Add an empty quad so our text gets cleared
		m_Mesh->AddQuad(GLVertex2D(vector2df(), GLColour(), vector2df()),
			GLVertex2D(vector2df(), GLColour(), vector2df()),
			GLVertex2D(vector2df(), GLColour(), vector2df()),
			GLVertex2D(vector2df(), GLColour(), vector2df()));
	}

	// Buffer our data
	if (m_Mesh->IsDirty())
		m_Mesh->Buffer();
}

bool GUIText::GetColour(GLColour& col, char c) const
{
	// Check for a valid colour flag
	switch (c)
	{
		case '0':
			col.r = 0; col.g = 0; col.b = 0;
			break;
		case '1':
			col.r = 255; col.g = 0; col.b = 0;
			break;
		case '2':
			col.r = 0; col.g = 255; col.b = 0;
			break;
		case '3':
			col.r = 0; col.g = 0; col.b = 255;
			break;
		case '4':
			col.r = 255; col.g = 0; col.b = 255;
			break;
		case '5':
			col.r = 0; col.g = 255; col.b = 255;
			break;
		case '6':
			col.r = 255; col.g = 255; col.b = 0;
			break;
		case '7':
			col.r = 255; col.g = 255; col.b = 255;
			break;
		case '8':
			col.r = 125; col.g = 125; col.b = 125;
			break;
		case '9':
			col.r = 75; col.g = 75; col.b = 75;
			break;
		default:
			return false;
			break;
	}

	// Return true if we have a new colour
	return true;
}

void GUIText::GetLineLengths(std::vector<int>& out) const
{
	// Get the length of each line in this text object
	// Used to centre text that has multiple lines
	size_t length = GetTextLength();
	char c = m_strText[0];
	GLColour col;
	int width = static_cast<int>(GetSize().x);
	int line = 0;

	for (size_t i = 0; i < length; c = m_strText[++i]) {
		// Sort out the colour
		if (c == '^' && m_strText[i + 1] != '\0')
		{
			if (GetColour(col, m_strText[i + 1]))
			{
				++i;
				continue;
			}
		}

		// Positional corrections inside text
		if (c == '%' && m_strText[i + 1] != '\0')
		{
			size_t j = i + 1;
			while (m_strText[j] != '\0')
			{
				if (m_strText[j] == '\\')
				{
					++j;
					break;
				}

				if (!isdigit(m_strText[j]))
					break;

				++j;
			}

			i += (j - i - 1);
			continue;
		}

		// Get the width of this character
		int advance = m_Font->GetCharacterInfo(c).advance;
		if (line + advance > width)
		{
			out.push_back(line);
			line = 0;

			continue;
		}
		line += advance;

		// Newlines
		if (c == '\n')
		{
			out.push_back(line);

			continue;
		}
	}

	// Add the final line length
	out.push_back(line);
}

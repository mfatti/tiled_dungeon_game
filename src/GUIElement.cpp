#include "GUIElement.h"
#include "GUIManager.h"
#include "InputManager.h"
#include "GUIText.h"

GUIElement::GUIElement(GUIManager* guiMan, GUIElement* parent)
	: m_Type(gui_element_type_count)
	, m_pGUIMan(guiMan)
	, m_pParent(parent)
	, m_Scale(1.f, 1.f)
	, m_bDead(false)
	, m_Texture(NULL)
	, m_iDepth(0)
	, m_bVisible(true)
	, m_uiQuadStart(0)
	, m_uiQuadCount(0)
	, m_bUpdated(true)
{
	// Set the default texture
	SetTexture("media/all_white.png");
}

GUIElement::~GUIElement()
{

}

GUIType GUIElement::GetType() const
{
	return m_Type;
}

void GUIElement::Update()
{
	if (m_bUpdated)
	{
		// Update our bounding box
		m_AABB.SetOffset(GetAbsolutePosition());

		// Remove the updated flag
		m_bUpdated = false;
	}
}

void GUIElement::ConstructMesh()
{
	// Reset our counters
	m_uiQuadStart = 0u;
	m_uiQuadCount = 0u;
	m_uiQuadCountTotal = 0u;

	// Build ourself if we are visible
	if (IsVisible())
		BuildElement();
}

void GUIElement::SetTexture(const GLTexture* tex)
{
	if (m_Texture && tex && m_Texture->ID() == tex->ID())
		return;
	
	if (GetGUIManager())
		GetGUIManager()->RebuildMesh(this);
	m_Texture = tex;
	if (GetGUIManager())
		GetGUIManager()->RebuildMesh(this);
}

void GUIElement::SetTexture(const std::string& filename)
{
	if (GetGUIManager())
	{
		const GLTexture* tex = &m_pGUIMan->GetTexture(filename);
		if (m_Texture && tex && m_Texture->ID() == tex->ID())
			return;
		
		GetGUIManager()->RebuildMesh(this);
		m_Texture = tex;
		GetGUIManager()->RebuildMesh(this);
	}
}

GUIElement* GUIElement::GetRoot()
{
	// If we have a parent, get their root
	if (GetParent())
		return GetParent()->GetRoot();

	// Otherwise we are the root
	return this;
}

void GUIElement::SetPosition(const vector2df& position)
{
	if (GetType() != GUIType::gui_text)
	{
		// Figure out how much we have moved by
		vector2df offset = position - m_Position;
		
		if (GetGUIManager())
			GetGUIManager()->MoveElement(this, offset);
	}

	// Update our position
	m_Position = position;
	m_bUpdated = true;
}

const vector2df& GUIElement::GetPosition() const
{
	return m_Position;
}

const vector2df GUIElement::GetAbsolutePosition() const
{
	if (m_pParent)
		return m_pParent->GetAbsolutePosition() + m_Position;

	return m_Position;
}

void GUIElement::SetScale(const vector2df& scale)
{
	m_Scale = scale;
	m_bUpdated = true;
}

const vector2df& GUIElement::GetScale() const
{
	return m_Scale;
}

void GUIElement::SetSize(const vector2df& size)
{
	m_Size = size;
	m_bUpdated = true;
}

const vector2df& GUIElement::GetSize() const
{
	return m_Size;
}

bool GUIElement::IsHovered() const
{
	return IsVisible() && m_AABB.PointInside(Singleton<InputManager>::Instance().GetCursorPosition());
}

void GUIElement::SetDepth(int depth)
{
	m_iDepth = depth;
}

const bool GUIElement::operator<(const GUIElement& element) const
{
	return (element.m_iDepth < m_iDepth);
}

void GUIElement::Kill()
{
	m_bDead = true;
}

bool GUIElement::IsDead() const
{
	return m_bDead;
}

void GUIElement::SetVisible(bool visible)
{
	bool current = IsVisible();
	m_bVisible = visible;
	
	if (GetGUIManager() && current != m_bVisible)
		GetGUIManager()->RebuildMesh(this);
}

bool GUIElement::IsVisible() const
{
	if (m_pParent)
        return (m_pParent->IsVisible() && m_bVisible);
    
    return m_bVisible;
}

unsigned int GUIElement::GetQuadStart() const
{
	return m_uiQuadStart;
}

unsigned int GUIElement::GetQuadCount() const
{
	return m_uiQuadCount;
}

unsigned int GUIElement::GetQuadCountTotal() const
{
	return m_uiQuadCountTotal;
}

void GUIElement::MarkForUpdate()
{
	m_bUpdated = true;
}

void GUIElement::SetBoundingBox(const vector2df& br, const vector2df& tl)
{
	m_AABB.SetAABB(br, tl);
}

unsigned int GUIElement::AddQuad(const GLVertex2D& ver1, const GLVertex2D& ver2, const GLVertex2D& ver3, const GLVertex2D& ver4, bool flip)
{
	if (!GetGUIManager())
		return 0u;

	// Get non-const GLVertex2D to work with
	GLVertex2D _ver1(ver1), _ver2(ver2), _ver3(ver3), _ver4(ver4);

	// Add a quad to the mesh
	const vector2df& offset = GetAbsolutePosition();
	_ver1.pos += offset;
	_ver2.pos += offset;
	_ver3.pos += offset;
	_ver4.pos += offset;

	if (m_uiQuadCount++ == 0)
	{
		m_uiQuadStart = GetGUIManager()->AddQuad(this, _ver1, _ver2, _ver3, _ver4);
		return m_uiQuadStart;
	}
	return GetGUIManager()->AddQuad(this, _ver1, _ver2, _ver3, _ver4);
}

void GUIElement::ChangeQuadColours(unsigned int pos, const GLColour& col1, const GLColour& col2, const GLColour& col3, const GLColour& col4)
{
	GUIManager* manager = GetGUIManager();
	if (manager)
	{
		manager->ChangeQuadColours(this, pos, col1, col2, col3, col4);
	}
}

void GUIElement::ChangeQuadPositions(GLuint pos, const vector2df& pos1, const vector2df& pos2, const vector2df& pos3, const vector2df& pos4)
{
	GUIManager* manager = GetGUIManager();
	if (manager)
	{
		manager->ChangeQuadPositions(this, pos, pos1, pos2, pos3, pos4);
	}
}

bool GUIElement::HasUpdated() const
{
	return m_bUpdated;
}

void GUIElement::SetType(GUIType type)
{
	m_Type = type;
}

void GUIElement::IncrementQuadCount()
{
	// If we have a parent, increase their quad count
	if (GetParent())
		GetParent()->IncrementQuadCount();

	++m_uiQuadCount;
}

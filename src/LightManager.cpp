#include "LightManager.h"
#include "DeltaTime.h"
#include "Functions.h"
#include "GLBase.h"
#include "Terrain.h"
#include <cstring>
#include <iostream>
#include <algorithm>

LightManager::LightManager(int size, int resolution)
	: m_Size(size)
	, m_NextIndex(0)
	, m_MaxLight(0)
	, m_Resolution(resolution)
	, m_Lights(new Light[size])
	, m_bUpdated(false)
	, m_RenderSize(0u)
	, m_Shader(nullptr)
	, m_Texture(0u)
	, m_LightFBO(0u)
	, m_LightMap(0u)
	, m_LightResolution(256)
	, m_PositionAttrib(0)
	, m_ColourAttrib(0)
	, m_TextureAttrib(0)
	, m_Mesh(new Mesh<GLVertex2D>)
{
	// Set up our shader
	std::string vertex_shader, fragment_shader;
	util::file_to_string("media/shaders/vert_lighting", vertex_shader);
	util::file_to_string("media/shaders/frag_lighting", fragment_shader);

	// Add this shader program to our vector
	GLShaderProgram* program = new GLShaderProgram(vertex_shader, fragment_shader);
	program->SetOrthoProjection(m_Resolution, m_Resolution);

	// Get our attribute and uniform positions
	program->Bind();
	m_PositionAttrib = program->GetAttribLocation("position");
	m_ColourAttrib = program->GetAttribLocation("colour");
	m_TextureAttrib = program->GetAttribLocation("texcoord");
	
	// Send default value in for the teximage uniform
	glUniform1i(program->GetUniformLocation("texImage"), 0);
	program->Unbind();

	m_Shader.reset(program);

	// Create our FBO and texture
	glGenFramebuffers(1, &m_LightFBO);

	// Create the light map texture
	glGenTextures(1, &m_LightMap);
	glBindTexture(GL_TEXTURE_2D, m_LightMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_Resolution, m_Resolution, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Attach the texture to the framebuffer object
	glBindFramebuffer(GL_FRAMEBUFFER, m_LightFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_LightMap, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Create our light texture
	const int res = m_LightResolution;
	unsigned char image_data[res * res * 4];
	memset(image_data, 0, res * res * 4);

	vector2di mid(res / 2, res / 2);
	for (int x = 0; x < res; ++x) {
		for (int y = 0; y < res; ++y) {
			float val = std::max(1.f - (mid.Distance(vector2di(x, y)) / (static_cast<float>(res) * 0.5f)), 0.f);
			image_data[x * res * 4 + y * 4] = 255 * val;
			image_data[x * res * 4 + y * 4 + 1] = 255 * val;
			image_data[x * res * 4 + y * 4 + 2] = 255 * val;
			image_data[x * res * 4 + y * 4 + 3] = 255 * val;
		}
	}

	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, res, res, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Add the quads to our mesh
	for (int i = 0; i < m_Size; ++i) {
		m_Mesh->AddQuad(GLVertex2D(),
		GLVertex2D(vector2df(), GLColour(), vector2df(1.f, 0.f)),
		GLVertex2D(vector2df(), GLColour(), vector2df(1.f, 1.f)),
		GLVertex2D(vector2df(), GLColour(), vector2df(0.f, 1.f)));
	}
}

LightManager::~LightManager()
{
	glDeleteFramebuffers(1, &m_LightFBO);
	glDeleteTextures(1, &m_LightMap);
	glDeleteTextures(1, &m_Texture);
	delete[] m_Lights;
}

// NOTE: Pointer returned from this WILL need to be checked for validity
Light* LightManager::CreateLight(const vector2df& pos, const GLColour& col, const vector2df& size, bool reserved)
{
	int index = GetNextIndex();
	if (index > -1)
	{
		Light* light = &m_Lights[index];
		light->position = pos;
		light->colour = col;
		light->size = size;
		light->fading = false;
		light->alive = true;
		light->reserved = reserved;

		if (index > m_MaxLight)
			m_MaxLight = index;

		return light;
	}

	return nullptr;
}

void LightManager::Update()
{
	const float scale = m_Resolution / AREA_SIZE;

	// Update our lights
	std::vector<GLVertex2D>& vertices = m_Mesh->GetVertexData();
	size_t update_size = 0;
	for (int i = 0; i <= m_MaxLight; ++i) {
		Light* light = &m_Lights[i];
		int vert = i * 4;
		if (light->alive)
		{
			// Update the render size
			m_RenderSize = (i + 1) * 6;
			
			// Shrink us if we are doing so
			if (light->fading)
			{
				float alpha = light->colour.a;
				util::Interpolate(alpha, 0.f, 0.5f, 10);
				light->colour.a = alpha;
				
				if (light->colour.a == 0)
					light->alive = false;
			}
			
			// Check opposite corners - will be able to detect both position and size changes
			if (light->position != light->prev_position || light->size != light->prev_size)
			{
				vertices[vert].pos = (light->position + vector2df(-light->size.x, -light->size.y)) * scale;
				vertices[vert + 1].pos = (light->position + vector2df(light->size.x, -light->size.y)) * scale;
				vertices[vert + 2].pos = (light->position + vector2df(light->size.x, light->size.y)) * scale;
				vertices[vert + 3].pos = (light->position + vector2df(-light->size.x, light->size.y)) * scale;
				m_bUpdated = true;
				
				light->prev_position = light->position;
				light->prev_size = light->size;
				update_size = static_cast<size_t>(vert + 4);
			}
			
			// Check colours - all vertices have same colour so easy check
			if (light->colour != vertices[vert].col)
			{
				vertices[vert].col = light->colour;
				vertices[vert + 1].col = light->colour;
				vertices[vert + 2].col = light->colour;
				vertices[vert + 3].col = light->colour;
				m_bUpdated = true;
				update_size = static_cast<size_t>(vert + 4);
			}
		}
		else
		{
			if (vertices[vert].col.a != 0)
			{
				// Needs killing
				vertices[vert].col.a = 0;
				vertices[vert + 1].col.a = 0;
				vertices[vert + 2].col.a = 0;
				vertices[vert + 3].col.a = 0;
				m_bUpdated = true;
				update_size = static_cast<size_t>(vert + 4);

				// Next free light slot is this one, assuming this has a lower index
				if (i < m_NextIndex)
					m_NextIndex = i;

				// If this is the max light, update max light index
				if (i == m_MaxLight)
				{
					for (int j = m_MaxLight; j > -1; --j) {
						if ((&m_Lights[j])->alive)
						{
							m_MaxLight = j;
							break;
						}
					}
				}
			}
		}
	}
	
	// Rebuild our mesh if needs be
	if (m_bUpdated || m_Mesh->IsDirty())
	{
		m_bUpdated = false;
		m_Mesh->Buffer(true, update_size);
	}
}

void LightManager::Render()
{
	// Bind the FBO and render our lights
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glViewport(0, 0, m_Resolution, m_Resolution);
	glBindFramebuffer(GL_FRAMEBUFFER, m_LightFBO);
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_CULL_FACE);

	m_Shader->Bind();
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	
	glBindBuffer(GL_ARRAY_BUFFER, m_Mesh->GetVBO());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Mesh->GetEBO());
	
	glEnableVertexAttribArray(m_PositionAttrib);
	glEnableVertexAttribArray(m_ColourAttrib);
	glEnableVertexAttribArray(m_TextureAttrib);
	
	GLsizei size = sizeof(GLVertex2D);
	glVertexAttribPointer(m_PositionAttrib, 2, GL_FLOAT, GL_FALSE, size, 0);
	glVertexAttribPointer(m_ColourAttrib, 4, GL_UNSIGNED_BYTE, GL_TRUE, size, (GLvoid *)(8));
	glVertexAttribPointer(m_TextureAttrib, 2, GL_SHORT, GL_FALSE, size, (GLvoid *)(12));
	
	glDrawElements(GL_TRIANGLES, m_RenderSize, GL_UNSIGNED_INT, 0);
	
	glDisableVertexAttribArray(m_PositionAttrib);
	glDisableVertexAttribArray(m_ColourAttrib);
	glDisableVertexAttribArray(m_TextureAttrib);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	m_Shader->Unbind();
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

int LightManager::GetNextIndex()
{
	int index(m_NextIndex);
	int count = 0;

	while ((&m_Lights[index])->alive || (&m_Lights[index])->reserved)
	{
		++index;
		++count;

		if (index >= m_Size)
		{
			index = 0;
		}

		if (count >= m_Size)
		{
			index = -1;
			break;
		}
	}

	if (index > -1)
		m_NextIndex = index;

	return index;
}
#ifndef SINGLETON_H_
#define SINGLETON_H_

#include "InputManager.h"

// Singleton class - a template class which allows easy creation / handling of singletons

template<typename T>
class Singleton {
public:
	Singleton(Singleton const&) = delete;
	void operator=(Singleton const&) = delete;

	static T& Instance()
	{
		static T instance;
		return instance;
	}

protected:
	Singleton() {};

private:

};

#endif
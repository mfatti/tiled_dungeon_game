#ifndef GUI_CHARACTER_PANE_
#define GUI_CHARACTER_PANE_

#include "GUIElement.h"
#include "GUIText.h"

// GUICharacterPane class - in-game character pane - background and name basically

class GUICharacterPane : public GUIElement {
public:
	GUICharacterPane(GUIManager* guiMan, const std::string& text, GUIElement* parent = NULL);
	
	void SetActive(bool active);
	void SetActions(int action, int max_action);
	void SetHealth(int health, int max_health);
	
protected:
	virtual void BuildElement();

private:
	GUIText*	m_Name;
	GUIText*	m_Health;
	GUIText*	m_Actions;
	int			m_ActionCount;
	int			m_MaxActionCount;
	int			m_StepCount;
	int			m_DefenseCount;
	int			m_HealthCount;
	int			m_MaxHealthCount;
	bool		m_Active;
};

#endif // GUI_CHARACTER_PANE_

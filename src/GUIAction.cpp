#include "GUIAction.h"
#include "GUIManager.h"

GUIAction::GUIAction(GUIManager* guiMan, GUIElement* parent)
	: GUIElement(guiMan, parent)
	, m_Image(nullptr)
	, m_Name(nullptr)
	, m_Enabled(true)
	, m_Active(false)
{
	// Set our type
	vector2df size(128.f, 128.f);
	SetType(GUIType::gui_action);
	SetSize(size);

	// Create our label
	GUIManager* manager = GetGUIManager();
	m_Name = manager->CreateText(vector2df(2.f, 64.f), "Move", "media/NotoSans-Bold.ttf:14", this);
	m_Name->SetSize(vector2df(size.x - 4.f, 16.f));
	m_Name->SetShadowed();
	m_Name->SetAlignment(TextAlignment::centre);
	SetBoundingBox(vector2df(), size);
	
	// Create the indicators image
	m_Image = manager->CreateImage("media/actions.png", vector2df(32.f, 0.f), vector2df(64.f, 64.f), this);
	m_Image->SetSubImage(vector2di(), vector2df(16.f, 16.f));
}

void GUIAction::SetAction(int id, const std::string& name)
{
	m_Name->SetText(name);
	m_Image->SetSubImage(vector2di(id, 0), vector2df(16.f, 16.f));
}

void GUIAction::SetActive(bool active)
{
	if (m_Active != active)
	{
		GLColour col1(active ? 255 : 0, 60);
		GLColour col2(active ? 255 : 0, 0);
		
		ChangeQuadColours(0, col2, col2, col1, col1);
		ChangeQuadColours(1, col1, col1, col1, col1);
		m_Active = active;
	}
}

void GUIAction::SetEnabled(bool enabled)
{
	if (enabled != m_Enabled)
	{
		m_Image->SetColour(GLColour(enabled ? 255 : 100, 255));
		m_Enabled = enabled;
	}
}

void GUIAction::BuildElement()
{
	// Add a pane
	GLColour col1(m_Active ? 255 : 0, 60);
	GLColour col2(m_Active ? 255 : 0, 0);
	
	const vector2df& size = GetSize();
	AddQuad(GLVertex2D(vector2df(), col2),
		GLVertex2D(vector2df(size.x, 0.f), col2),
		GLVertex2D(vector2df(size.x, size.y * 0.4f), col1),
		GLVertex2D(vector2df(0.f, size.y * 0.4f), col1));
	AddQuad(GLVertex2D(vector2df(0.f, size.y * 0.4f), col1),
		GLVertex2D(vector2df(size.x, size.y * 0.4f), col1),
		GLVertex2D(vector2df(size.x, size.y), col1),
		GLVertex2D(vector2df(0.f, size.y), col1));
}

#include "GUITextBox.h"
#include "GUIManager.h"
#include "InputManager.h"
#include "DeltaTime.h"

GUITextBox::GUITextBox(GUIManager* guiMan, const std::string& font, const vector2df& size, GUIElement* parent)
	: GUIElement(guiMan, parent)
	, m_pText(NULL)
	, m_pHint(NULL)
	, m_bNumbersOnly(false)
	, m_CharLimit(0)
	, m_bFocused(false)
	, m_CursorPosition(0)
{
	// Set our type
	SetType(GUIType::gui_textbox);

	// Create our text and our hint
	m_pText = GetGUIManager()->CreateText(vector2df(4.f, 2.f), "", font, this);
	m_pHint = GetGUIManager()->CreateText(vector2df(4.f, 2.f), "", font, this);
	SetSize(size);
	SetBoundingBox(vector2df(), size);
	m_pText->SetSize(size);
	m_pHint->SetSize(size);
}

GUITextBox::~GUITextBox()
{
	// Kill our text
	m_pText->Kill();
	m_pHint->Kill();
}

void GUITextBox::Update()
{
	// Unfocus us if we're not visible or the user clicks outside of us
	if (!IsVisible())
		SetFocus(false);

	// Call base class Update
	GUIElement::Update();
}

void GUITextBox::SetCharacterLimit(size_t limit)
{
	m_CharLimit = limit;
}

void GUITextBox::SetNumericOnly(bool numeric)
{
	m_bNumbersOnly = numeric;
}

void GUITextBox::SetHint(const std::string& text)
{
	m_pHint->SetText(std::string("^9").append(text));
	m_pHint->SetVisible(m_pText->GetTextLength() < 1);
}

void GUITextBox::SetFocus(bool focus)
{
	bool bChanged = (m_bFocused != focus);
	m_bFocused = focus;

	if (bChanged)
	{
		if (focus)
			m_pHint->SetVisible(false);
		else if (m_pText->GetTextLength() == 0)
			m_pHint->SetVisible();
	}
}

bool GUITextBox::IsFocused() const
{
	return m_bFocused;
}

void GUITextBox::CharInput(unsigned int character)
{
	// Input a character
	char c = static_cast<char>(character);
	if ((!m_bNumbersOnly || (isdigit(c) || c == '-' || c == '.')) && (m_CharLimit == 0 || m_pText->GetTextLength() < m_CharLimit))
	{
		bool bEnd = (m_CursorPosition == m_pText->GetTextLength());

		m_pText->AppendText(std::string(1, c), m_CursorPosition);
		m_pHint->SetVisible(false);

		if (bEnd)
			++m_CursorPosition;
	}
}

void GUITextBox::EraseCharacter()
{
	/// TODO: Erase based on cursor position
	if (IsFocused() && m_pText->GetTextLength() > 0)
	{
		// Handle erase delaying
		if (m_fEraseDelay == 0.f || m_fEraseDelay > 3.f)
		{
			if (m_CursorPosition == m_pText->GetTextLength())
			{
				m_pText->SetText(m_pText->GetText().substr(0, m_pText->GetTextLength() - 1));
				--m_CursorPosition;
			}
		}

		m_fEraseDelay += Singleton<DeltaTime>::Instance().GetDelta() * 10.f;
	}
}

void GUITextBox::ResetEraseDelay()
{
	m_fEraseDelay = 0.f;
}

const std::string& GUITextBox::GetText() const
{
	return m_pText->GetText();
}

void GUITextBox::BuildElement()
{
	// Add a box and a cursor
	const vector2df& size = GetSize();
	AddQuad(GLVertex2D(vector2df(), GLColour(0, 100)),
		GLVertex2D(vector2df(size.x, 0.f), GLColour(0, 100)),
		GLVertex2D(vector2df(size.x, size.y), GLColour(0, 100)),
		GLVertex2D(vector2df(0.f, size.y), GLColour(0, 100)));
}

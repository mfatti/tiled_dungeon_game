#include "GUILabel.h"
#include "GUIManager.h"
#include "InputManager.h"
#include <algorithm>

GUILabel::GUILabel(GUIManager* guiMan)
	: GUIElement(NULL, NULL)
	, m_Title(new GUIText(NULL, "", this))
	, m_Description(new GUIText(NULL, "", this))
	, m_Mesh(new Mesh<GLVertex2D>)
{
	// Set our type
	SetType(GUIType::gui_label);

	// Our GUIManager pointer is NULL as we don't need to access anything it provides
	m_Title->SetAlignment(TextAlignment::centre);
	m_Title->SetPosition(vector2df(8.f, 0.f));
	m_Description->SplitByWord();

	// Set the fonts
	m_Title->SetFont(&guiMan->GetFont("media/PressStart2P.ttf:14"));
	m_Title->SetShadowed();
	m_Description->SetFont(&guiMan->GetFont("media/PressStart2P.ttf:12"));

	// Set us to be invisible
	SetVisible(false);
}

void GUILabel::Update()
{
	// If we're visible move us with the mouse
	if (IsVisible())
	{
		SetPosition(GET_CURSOR_POSITION);
		m_Title->SetPosition(m_Title->GetPosition());
		m_Description->SetPosition(m_Description->GetPosition());
	}
	
	if (HasUpdated())
	{
		// Construct our transformation matrix
		m_Transformation.Identity();
		m_Transformation.Scale(GetScale());
		m_Transformation.SetTranslation(GetAbsolutePosition());
	}

	// Call base class Update
	GUIElement::Update();

	// Update our text
	m_Title->Update();
	m_Description->Update();
}

void GUILabel::SetTitle(const std::string& text)
{
	if (text.empty() && !m_Title->GetText().empty())
	{
		// Clearing down the label, hide it
		m_Title->SetText("");
		SetVisible(false);
		m_Mesh->SetDirty();
	}
	else if (!text.empty() && m_Title->GetText() != text)
	{
		// Setting the label
		m_Title->SetText(text);
		SetVisible(true);
		m_Mesh->SetDirty();
	}
}

void GUILabel::SetDescription(const std::string& text)
{
	m_Description->SetText(text);
}

const Mesh<GLVertex2D>* GUILabel::GetMesh() const
{
	return m_Mesh.get();
}

const GUIText* GUILabel::GetTitle() const
{
	return m_Title.get();
}

const GUIText* GUILabel::GetDescription() const
{
	return m_Description.get();
}

const matrix4f& GUILabel::GetTransformationMatrix() const
{
	return m_Transformation;
}

void GUILabel::BuildElement()
{
	// Clear our mesh
	m_Mesh->Clear();
	
	// Create our label
	float fWidth = std::min(std::max(std::max(m_Description->GetTextWidth(), m_Title->GetTextWidth()), 160.f), 480.f);
	m_Title->SetSize(vector2df(fWidth, 0.f));
	m_Description->SetSize(vector2df(fWidth, 0.f));
	
	float fHeight = m_Title->GetTextHeight() + m_Description->GetTextHeight() + 8.f;
	SetSize(vector2df(fWidth + 16.f, fHeight));
	m_Description->SetPosition(vector2df(8.f, m_Title->GetTextHeight() + 4.f));

	// Build the background
	vector2df size(fWidth + 16.f, fHeight);
	m_Mesh->AddQuad(GLVertex2D(vector2df(), GLColour(255, 70)),
		GLVertex2D(vector2df(size.x, 0.f), GLColour(255, 70)),
		GLVertex2D(vector2df(size.x, size.y), GLColour(255, 70)),
		GLVertex2D(vector2df(0.f, size.y), GLColour(255, 70)));

	m_Mesh->AddQuad(GLVertex2D(vector2df(2.f, 2.f), GLColour(0, 180)),
		GLVertex2D(vector2df(size.x - 2.f, 2.f), GLColour(0, 180)),
		GLVertex2D(vector2df(size.x - 2.f, size.y - 2.f), GLColour(0, 180)),
		GLVertex2D(vector2df(2.f, size.y - 2.f), GLColour(0, 180)));

	if (m_Mesh->IsDirty())
		m_Mesh->Buffer();

	// Build our text
	m_Title->ConstructMesh();
	m_Description->ConstructMesh();
}

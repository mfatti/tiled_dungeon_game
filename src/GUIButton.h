#ifndef GUI_BUTTON_H_
#define GUI_BUTTON_H_

#include <functional>
#include "GUIElement.h"
#include "GUIText.h"

// GUIButton class - WHY DIDN'T I HAVE ONE OF THESE PROGRAMMED BEFORE THE JAM?!

class GUIButton : public GUIElement {
public:
	GUIButton(GUIManager* guiMan, const std::string& text, const std::string& font, const vector2df& size, GUIElement* parent = NULL);

	virtual void Update();

	void SetFunction(/*bool(*func)(GUIElement*)*/const std::function<bool(GUIElement*)>& func);
	bool OnClick();

	void SetText(const std::string& text);

protected:
	virtual void BuildElement();

private:
	GUIText*	m_pGUILabel;

	//bool(*m_Function)(GUIElement*);
	std::function<bool(GUIElement*)> m_Function;
};
 
#endif // GUI_BUTTON_H_

#ifndef UNIT_CONTROLLER_MANAGER_H_
#define UNIT_CONTROLLER_MANAGER_H_

#include "InputManager.h"
#include "UnitController.h"
#include "EnemyController.h"
#include "PlayerController.h"

// UnitControllerManager class - manages unit controllers, handles inputs and whatnot

class UnitControllerManager : public InputObject {
public:
	UnitControllerManager();

	void Update();
	
	void AddUnitController(UnitController* controller);
	UnitController* GetUnitController(size_t id);
	size_t GetCurrentIndex() const { return m_CurrentIndex; }
	
	void NextController();
	
	virtual bool MouseEvent(int button, int action, int mods);
	virtual bool KeyboardEvent(int key, int scancode, int action, int mods);
	virtual bool CharEvent(unsigned int codepoint);
	virtual bool ScrollEvent(double x, double y);

protected:

private:
	std::vector<std::shared_ptr<UnitController>>	m_UnitControllers;
	UnitController*									m_CurrentController;
	size_t											m_CurrentIndex;
};

#endif // UNIT_CONTROLLER_MANAGER_H_

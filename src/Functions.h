#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include "DeltaTime.h"

// Misc functions needed throughout the code (where they are needed in more than one place)

namespace util
{
	void VariableToBuffer(void* var, size_t size, std::vector<unsigned char>& out);

	template<typename T>
	void BufferToVariable(const unsigned char* data, T& out)
	{
		// Conversion union
		union conv {
			T data;
			unsigned char buffer[sizeof(T)];
		} buffer;

		// Get the data
		memcpy(buffer.buffer, data, sizeof(T));

		// Set our value
		out = buffer.data;
	}

	std::streampos FileSize(std::fstream& file);

	template<typename T>
	void Interpolate(T& current, T result, float smooth = 5.f, float diff = 0.1f, float max_change = 1.f)
	{
		T fDif = result - current;
		float delta = Singleton<DeltaTime>::Instance().GetDelta();

		if (abs(fDif) < diff)
		{
			current = result;
		}
		else
		{
			current += std::min((fDif / smooth) * (T)delta, max_change);
		}
	}

	float RandomBounds(float lower, float upper, float precision = 100.f);

	bool file_to_string(const std::string& filename, std::string& out);
}

#endif // FUNCTIONS_H_

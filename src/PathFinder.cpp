#include "PathFinder.h"
#include <algorithm>

PathFinder::PathFinder(const vector2di& size)
	: m_Walls(size.x * size.y, false)
	, m_Size(size)
{
	
}

void PathFinder::SetWall(const vector2di& pos, bool wall)
{
	if (pos.x >= 0 && pos.x < m_Size.x && pos.y >= 0 && pos.y < m_Size.y)
	{
		m_Walls.at(pos.x * m_Size.y + pos.y) = wall;
	}
}

void PathFinder::ClearWalls()
{
	size_t size = m_Size.x * m_Size.y;
	for (size_t i = 0; i < size; ++i) {
		m_Walls.at(i) = false;
	}
}

bool PathFinder::FindPath(const vector2di& start, const vector2di& end, std::vector<vector2di>& path, int max_calculations)
{
	static const int directions = 8;
	static const vector2di direction[directions] = {
		vector2di(0, 1), vector2di(1, 0), vector2di(0, -1), vector2di(-1, 0),
		vector2di(1, 1), vector2di(1, -1), vector2di(-1, -1), vector2di(-1, 1)
	};
	std::set<std::shared_ptr<Path::Node>> open;
	std::vector<std::shared_ptr<Path::Node>> keep_alive;
	bool closed_nodes[m_Size.x * m_Size.y] = {false};
	
	open.insert(std::shared_ptr<Path::Node>(new Path::Node(start)));
	
	// If our destination is a wall, remove it temporarily (set it back at the end) so a path can be calculated
	bool end_is_wall = GetWall(end);
	SetWall(end, false);
	
	// Go through each node in the open set and get next possible paths from each node
	int count = 0;
	while (!open.empty())
	{
		// Stop us from doing too much work (if specified)
		if (max_calculations > 0 && count++ >= max_calculations)
			break;
		
		// Get the node with the lowest score
		std::shared_ptr<Path::Node> current = *open.begin();
		for (auto& node : open) {
			if (node->score() <= current->score())
			{
				current = node;
			}
		}
		
		// If we have reached the end then this has been a success
		if (current->position == end)
		{
			// Reconstruct the path and leave this function
			Path::Node* path_node = current.get();
			while (path_node)
			{
				path.push_back(path_node->position);
				path_node = path_node->previous;
			}
			
			// Reverse the path
			std::reverse(path.begin(), path.end());
			
			// Set the wall at the end back to what it was
			SetWall(end, end_is_wall);
			
			return true;
		}
		
		// Mark this node as closed and add it to a temporary keep_alive vector
		// This is so we can reconstruct our path correctly at the end
		closed_nodes[current->position.x * m_Size.y + current->position.y] = true;
		keep_alive.push_back(current);
		open.erase(std::find(open.begin(), open.end(), current));
		
		// Check each adjacent node
		for (int i = 0; i < directions; ++i) {
			vector2di check_position(current->position + direction[i]);
			
			// If we have found a wall or a previously evaluated node, move on
			if (GetWall(check_position) || closed_nodes[check_position.x * m_Size.y + check_position.y])
			{
				continue;
			}
			
			// Stop pathfinding being able to cut corners
			if (i >= 4)
			{
				if (GetWall(current->position.x + direction[i].x, current->position.y) &&
					GetWall(current->position.x, current->position.y + direction[i].y))
				{
					continue;
				}
			}
			
			// When i < 4 we are checking horizontal movements, we want these to give a lower score
			int cost = current->path_score + (i < 4 ? 10 : 14);
			
			Path::Node* next_node = FindNodeInSet(open, check_position);
			if (!next_node)
			{
				// No node found, add in a node to be checked
				next_node = new Path::Node(check_position, current.get());
				next_node->path_score = cost;
				next_node->heuristic = Heuristic(check_position, end);
				open.insert(std::shared_ptr<Path::Node>(next_node));
			}
			else if (next_node->path_score > cost)
			{
				// Better path found to this node, adjust its path
				next_node->previous = current.get();
				next_node->path_score = cost;
			}
		}
	}
	
	// If we reach this point we have failed to find a path
	SetWall(end, end_is_wall);
	return false;
}

void PathFinder::GetFillArea(const vector2di& start, int value, std::vector<vector2di>& area) const
{
	// Do a BFS fill and output to the area
	std::set<std::shared_ptr<Path::FillNode>> nodes;
	nodes.insert(std::shared_ptr<Path::FillNode>(new Path::FillNode(start, value + 1)));
	std::vector<int> done(m_Size.x * m_Size.y, 0);
	
	static const int directions = 8;
	static const vector2di direction[directions] = {
		vector2di(0, 1), vector2di(1, 0), vector2di(0, -1), vector2di(-1, 0),
		vector2di(1, 1), vector2di(1, -1), vector2di(-1, -1), vector2di(-1, 1)
	};
	
	while (!nodes.empty())
	{
		// Add this to the area and done map
		std::shared_ptr<Path::FillNode> current = *nodes.begin();
		for (auto& node : nodes) {
			if (node->value >= current->value)
			{
				current = node;
			}
		}
				
		done.at(current->position.x * m_Size.y + current->position.y) = current->value;
		area.push_back(current->position);
		
		nodes.erase(std::find(nodes.begin(), nodes.end(), current));
		
		// Check surrounding tiles
		if (current->value > 1)
		{
			for (int i = 0; i < directions; ++i) {
				vector2di check_position(current->position + direction[i]);
				
				if (!GetWall(check_position) &&
					done.at(check_position.x * m_Size.y + check_position.y) == 0 &&
					!FindNodeInSet(nodes, check_position))
				{
					nodes.insert(std::shared_ptr<Path::FillNode>(new Path::FillNode(check_position, current->value - 1)));
				}
			}
		}
	}
}

int PathFinder::Heuristic(const vector2di& one, const vector2di& two) const
{
	// Heuristic function for this pathfinder is going to be Chebyshev as our movement is like a chessboard (diagonal costs 1 move)
	return std::max(abs(one.x - two.x), abs(one.y - two.y));
}

template<class T> T* PathFinder::FindNodeInSet(std::set<std::shared_ptr<T>>& node_set, const vector2di& position) const
{
	for (auto& node : node_set) {
		if (node->position == position)
			return node.get();
	}
	
	return nullptr;
}

template Path::Node* PathFinder::FindNodeInSet(std::set<std::shared_ptr<Path::Node>>& node_set, const vector2di& position) const;
template Path::FillNode* PathFinder::FindNodeInSet(std::set<std::shared_ptr<Path::FillNode>>& node_set, const vector2di& position) const;
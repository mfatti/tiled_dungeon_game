#ifndef GUI_IMAGE_H_
#define GUI_IMAGE_H_

#include "GUIElement.h"

// GUIImage class - a simple image that can be easily changed

class GUIImage : public GUIElement {
public:
	GUIImage(GUIManager* guiMan, const std::string& texture, const vector2df& size, GUIElement* parent = NULL);

	void SetColour(const GLColour& col);

	void SetSubImage(const vector2di& offset = vector2di(), const vector2df& size = vector2df());

protected:
	virtual void BuildElement();

private:
	vector4df	m_ImageSubset;
};

#endif // GUI_IMAGE_H_

#ifndef TERRAIN_DATA_H_
#define TERRAIN_DATA_H_

#include <cstring>

// Defines relating to the terrain data - ie struct definitions

// Tile enum
// These make up the base ground tiles
namespace nsTile
{
	enum Tile : unsigned char {
		empty = 0,
		stone_wall_1,
		stone_wall_2,
		wooden_wall_1,
		cave_wall_1,
		future_wall_1,
		flesh_wall_1,
		jungle_wall_1,
		secret_wall_1,

		last_wall_type, // Used for stuff

		water,
		lava,
		slime,
		mana,
		sludge,
		blood,
		grass,
		stone_1,
		cave_1,
		dirt,
		sand,
		quick_sand,
		wood,
		dark_wood,
		carpet_red,
		carpet_blue,
		carpet_green,
		carpet_grey,
		molten_stone,
		flesh,
		pit,
		secret,
		future
	};
}

// TileEntity enum
// These are tiles which are entities existing on top of the ground tiles
// These can be represented as a 3d voxel mesh or just a flat 2d sprite
namespace nsTileEntity
{
	enum TileEntity : unsigned char {
		empty = 0,
		next_level,
		wooden_bridge,
		stone_bridge,
		/// TODO: other ores
		chest,
		sign,
		lever,
		trap_door,
		stone_door
	};
}

// TileEntity struct
#pragma pack(1)
struct TileEntity {
	nsTileEntity::TileEntity type;
	unsigned char state;
	unsigned char rot;
	unsigned char data[5];

	TileEntity() : type(nsTileEntity::empty), state(0), rot(0) { memset(data, 0, 5); }
	TileEntity(nsTileEntity::TileEntity t, unsigned char s = 0, unsigned char r = 0, unsigned char* d = 0) : type(t), state(s), rot(r) { if (d) memcpy(data, d, 5); }
};
#pragma pack()

// Tile struct
#pragma pack(1)
struct Tile {
	nsTile::Tile tile;

	Tile() : tile(nsTile::empty) {}
	//Tile(nsTile::Tile t) : tile(t) { memset(walls, nsTileWall::empty, nsTileWall::wall_count); }
};
#pragma pack()

#endif

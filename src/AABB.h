#ifndef _AABB_H_
#define _AABB_H_

#include "Vector.h"

class ViewFrustum;

// AABB class - 3d axis-aligned bounding box

class AABB {
public:
	AABB() : bottomLeft(), topRight() {}
	AABB(const AABB& aabb) : bottomLeft(aabb.bottomLeft), topRight(aabb.topRight) {}
	AABB(const vector3df& bl, const vector3df& tr) : bottomLeft(bl), topRight(tr) {}

	void SetAABB(const vector3df& bl, const vector3df& tr);

	bool InViewFrustum(const ViewFrustum& frustum) const;
	bool QuickInViewFrustum(const ViewFrustum& frustum, const vector3df& pos) const;

	vector3df bottomLeft;
	vector3df topRight;
	float size;

protected:

private:
	float halfsize;
	vector3df midPoint;
};

// AABB2D class - 2d axis-align bounding box

class AABB2D {
public:
	AABB2D() : m_BottomRight(), m_TopLeft() {}
	AABB2D(const AABB2D& aabb) : m_BottomRight(aabb.m_BottomRight), m_TopLeft(aabb.m_TopLeft) {}
	AABB2D(const vector2df& br, const vector2df& tl) : m_BottomRight(br), m_TopLeft(tl) {}

	void SetAABB(const vector2df& br, const vector2df& tl);
	void SetOffset(const vector2df& offset);

	bool PointInside(const vector2df& pos) const;

protected:

private:
	vector2df m_BottomRight;
	vector2df m_TopLeft;
	vector2df m_Offset;
};

#endif
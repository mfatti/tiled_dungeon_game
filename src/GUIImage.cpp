#include "GUIImage.h"
#include "GUIManager.h"

GUIImage::GUIImage(GUIManager* guiMan, const std::string& texture, const vector2df& size, GUIElement* parent)
	: GUIElement(guiMan, parent)
	, m_ImageSubset(0.f, 1.f, 1.f, 0.f)
{
	// Set our type
	SetType(GUIType::gui_image);

	// Set our size
	SetSize(size);

	// Set our texture
	SetTexture(texture);
}

void GUIImage::SetColour(const GLColour& col)
{
	ChangeQuadColours(0, col, col, col, col);
}

void GUIImage::SetSubImage(const vector2di& offset, const vector2df& size)
{
	// Calculate a sub image
	vector4df old(m_ImageSubset);
	if (offset == vector2df() && size == vector2di())
	{
		// We need the whole image
		m_ImageSubset.Set(0.f, 1.f, 1.f, 0.f);
	}
	else
	{
		// Figure out a subset
		const vector2df imageSize = GetTexture()->Dimensions();
		vector2df sub(size / imageSize);
		m_ImageSubset.Set(offset.x * sub.x, 1.f - (offset.y * sub.y), (offset.x + 1) * sub.x, 1.f - (offset.y + 1) * sub.y);
	}
	
	// We will need remaking
	if (old != m_ImageSubset)
		GetGUIManager()->RebuildMesh(this);
}

void GUIImage::BuildElement()
{
	// Simply add a quad
	const vector2df& size = GetSize();
	AddQuad(GLVertex2D(vector2df(), GLColour(), vector2df(m_ImageSubset.x, m_ImageSubset.y)),
		GLVertex2D(vector2df(size.x, 0.f), GLColour(), vector2df(m_ImageSubset.z, m_ImageSubset.y)),
		GLVertex2D(vector2df(size.x, size.y), GLColour(), vector2df(m_ImageSubset.z, m_ImageSubset.w)),
		GLVertex2D(vector2df(0.f, size.y), GLColour(), vector2df(m_ImageSubset.x, m_ImageSubset.w)));
}

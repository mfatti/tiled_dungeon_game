#ifndef STATUS_EFFECT_H
#define STATUS_EFFECT_H

#include "ParticleEmitter.h"
#include <string>

class Unit;
class SceneManager;

namespace nsStatusEffect
{
	enum EffectType {
		none = 0,
		
		// Negative effects
		fire,
		ice,
		poison,
		life_steal,
		life_transfer,
		
		// Positive effects
		heal,
		shield,
		damage_transfer,
		buff_damage,
		buff_health,
		buff_movement
	};
}

// StatusEffect class - base class for all status effects, ie shield, poison, etc

class StatusEffect {
public:
	StatusEffect(SceneManager* sceneman, Unit* owner, Unit* target, const std::string& name, int life, int rounds, float modifier, nsStatusEffect::EffectType type);
	virtual ~StatusEffect();

	virtual bool TurnStart();

	// Override these and return true if the effect applies to the event
	virtual bool OnApply() { return false; }
	virtual bool OnKill() { return false; }
	virtual bool OnDamage(int& value, Unit* other = 0) { return false; }
	virtual bool OnHeal(int& value, Unit* other = 0) { return false; }
	virtual bool OnAbilityUse(int& value, Unit* other = 0) { return false; }
	virtual bool OnMovement(int& value) { return false; }

	void SetOwner(Unit* owner) { m_Owner = owner; }
	void SetTarget(Unit* target) { m_Target = target; }

	void UpdatePosition();

	bool Damage() { return (m_Life > 0 && --m_Life == 0); }
	bool IsDead() const { return (m_Life == 0 || m_RoundsLeft == 0); }

	Unit* GetOwner() { return m_Owner; }
	Unit* GetTarget() { return m_Target; }
	float GetModifier() { return m_Modifier; }
	nsStatusEffect::EffectType GetType() { return m_Type; }
	const std::string& GetName() { return m_Name; }

	static StatusEffect* CreateStatusEffect(nsStatusEffect::EffectType type, SceneManager* sceneman, Unit* owner, Unit* target, int life, int rounds, float modifier);

protected:
	SceneManager* GetSceneManager() { return m_SceneMan; }

private:
	SceneManager*				m_SceneMan;
	Unit*						m_Owner;
	Unit*						m_Target;
	int							m_Life;
	int							m_RoundsLeft;
	float						m_Modifier;
	ParticleEmitter*			m_TargetEmitter;
	ParticleEmitter*			m_OwnerEmitter;
	std::string					m_Name;
	nsStatusEffect::EffectType	m_Type;
};

#endif // STATUS_EFFECT_H

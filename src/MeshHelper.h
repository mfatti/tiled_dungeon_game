#ifndef MESH_HELPER_H_
#define MESH_HELPER_H_

#include <string>
#include "Vector.h"

class MeshBase;

// Helper class for mesh loading

class MeshHelper {
public:
	static MeshBase* CreateMesh(const std::string& filename);

protected:

private:

};

#endif

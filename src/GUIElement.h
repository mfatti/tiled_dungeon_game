#ifndef GUI_ELEMENT_H_
#define GUI_ELEMENT_H_

#include "AABB.h"
#include "GLBase.h"
#include <string>

class GUIManager;
class GUIElement;
class GLTexture;

// GUIType enum
enum GUIType {
	gui_image,
	gui_inventory,
	gui_label,
	gui_pane,
	gui_text,
	gui_textbox,
	gui_crafting,
	gui_button,
	gui_character_pane,
	gui_action,

	gui_element_type_count
};

// GUIElement class - this is the base class for all GUI elements, essentially a SceneNode but for GUI

class GUIElement {
public:
	GUIElement(GUIManager* guiMan, GUIElement* parent = NULL);
	virtual ~GUIElement();

	GUIType GetType() const;

	virtual void Update();
	void ConstructMesh();

	void SetTexture(const GLTexture* tex);
	void SetTexture(const std::string& filename);
	const GLTexture* GetTexture() const { return m_Texture; }

	GUIElement* GetParent() { return m_pParent; }
	GUIElement* GetRoot();

	void SetPosition(const vector2df& position);
	const vector2df& GetPosition() const;
	const vector2df GetAbsolutePosition() const;

	void SetScale(const vector2df& scale);
	const vector2df& GetScale() const;

	virtual void SetSize(const vector2df& size);
	const vector2df& GetSize() const;

	bool IsHovered() const;

	void SetDepth(int depth);

	const bool operator<(const GUIElement& element) const;

	void Kill();
	bool IsDead() const;

	void SetVisible(bool visible = true);
	bool IsVisible() const;

	unsigned int GetQuadStart() const;
	unsigned int GetQuadCount() const;
	unsigned int GetQuadCountTotal() const;

	void MarkForUpdate();

	GUIManager* GetGUIManager() { return m_pGUIMan; }
	
protected:
	virtual void BuildElement() = 0;

	void SetBoundingBox(const vector2df& br, const vector2df& rl);

	unsigned int AddQuad(const GLVertex2D& ver1, const GLVertex2D& ver2, const GLVertex2D& ver3, const GLVertex2D& ver4, bool flip = false);
	void ChangeQuadColours(unsigned int pos, const GLColour& col1, const GLColour& col2, const GLColour& col3, const GLColour& col4);
	void ChangeQuadPositions(GLuint pos, const vector2df& pos1, const vector2df& pos2, const vector2df& pos3, const vector2df& pos4);

	bool HasUpdated() const;

	void SetType(GUIType type);

private:
	GUIType								m_Type;
	GUIManager*							m_pGUIMan;
	GUIElement*							m_pParent;
	vector2df							m_Position;
	vector2df							m_Scale;
	vector2df							m_Size;
	bool								m_bDead;
	AABB2D								m_AABB;
	const GLTexture*					m_Texture;
	int									m_iDepth;
	bool								m_bVisible;
	unsigned int						m_uiQuadStart;
	unsigned int						m_uiQuadCount;
	unsigned int						m_uiQuadCountTotal;
	bool								m_bUpdated;

	void IncrementQuadCount();
};

#endif // GUI_ELEMENT_H_
